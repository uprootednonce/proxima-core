#![allow(unused)]

use std::collections::*;
use std::iter;
use std::net;
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use std::sync::Arc;

use async_trait::async_trait;
use bytes::{buf::*, BytesMut};
use futures;
use futures::prelude::*;
use tokio::net as tnet;
use tokio::prelude::*;
use tokio::sync::{mpsc, oneshot, Mutex, MutexGuard, Notify};
use tokio::task;

use proxcomm::crypto;
use proxcomm::p2p;

pub struct PeeringConfig {
    static_key: crypto::Pubkey,
    listen_addrs: Vec<(net::IpAddr, u16)>,
}

impl PeeringConfig {
    pub fn from_ident(id: &crypto::Identity) -> PeeringConfig {
        PeeringConfig {
            static_key: id.encryption_key.to_pubkey(),
            listen_addrs: Vec::new(),
        }
    }

    pub fn add_listen(&mut self, addr: net::IpAddr, port: u16) {
        self.listen_addrs.push((addr, port));
    }
}

#[derive(Clone)]
pub struct PeerManager(Arc<Mutex<PeerManagerInner>>);

impl PeerManager {
    pub fn new(conf: PeeringConfig) -> PeerManager {
        let pmi = PeerManagerInner::new(conf);
        PeerManager(Arc::new(Mutex::new(pmi)))
    }

    // TODO Check to see if we actually need this.
    pub async fn lock_inner(&self) -> MutexGuard<'_, PeerManagerInner> {
        self.0.lock().await
    }
}

pub struct PeerManagerInner {
    transport_caps: Vec<p2p::TransportCapability>,

    // The u64 is the peer index.
    peers_tbl: Mutex<HashMap<u64, PeerConn>>,
    next_peer_idx: AtomicU64,
}

impl PeerManagerInner {
    pub fn new(conf: PeeringConfig) -> PeerManagerInner {
        let caps = conf
            .listen_addrs
            .iter()
            .cloned()
            .map(|(ip, port)| p2p::TransportCapability::TcpInbound { ip, port })
            .chain(iter::once(p2p::TransportCapability::TcpOutbound))
            .collect();

        PeerManagerInner {
            transport_caps: caps,

            peers_tbl: Mutex::new(HashMap::new()),
            next_peer_idx: AtomicU64::new(1),
        }
    }

    pub async fn connect_tcp(&self, sock: net::SocketAddr) -> Result<p2p::PeerTicket, p2p::Error> {
        let stream = tnet::TcpStream::connect(&sock)
            .map_err(|_| p2p::Error::NetworkFailure)
            .await?;
        let stop_sig = Arc::new(Notify::new());
        let dc_sig = Arc::new(Notify::new());

        // TODO Handshake things?

        let idx = self.next_peer_idx.fetch_add(1, Ordering::Relaxed);

        // TODO Make this set up the thing to reconnect.
        let pc = init_peer(stream, stop_sig.clone(), dc_sig.clone(), idx).await?;
        let pa = p2p::PeerAddr::TcpIp { sock };
        let tkt = p2p::PeerTicketData::from_parts(pa, stop_sig, dc_sig, pc);

        Ok(tkt)
    }

    /// Returns a long-lived future that listens on the thread and accepts new
    /// connections.  Dropping this future will stop listening, but will keep
    /// active peer connections open.  This future should probably be wrapped in
    /// some control logic and spawned as its own task.
    pub async fn listen(
        &self,
        sock_addr: net::SocketAddr,
        stop_sig: Arc<Notify>,
        handler: p2p::ConnHandler,
    ) -> Result<(), p2p::Error> {
        let mut sock = tnet::TcpListener::bind(sock_addr)
            .map_err(|_| p2p::Error::NetworkFailure)
            .await?;

        let accept_fut = do_accept_worker(&mut sock, self, handler);
        let stop_fut = stop_sig.notified();

        futures::pin_mut!(accept_fut);
        futures::pin_mut!(stop_fut);

        match future::select(accept_fut, stop_fut).await {
            future::Either::Left((Ok(_), _)) => {
                eprintln!("[peerman] shouldn't be able to get here");
            }
            future::Either::Left((Err(e), _)) => {
                eprintln!("[peerman] io error in accept: {:?}", e);
            }
            future::Either::Right(_) => {
                eprintln!("[peerman] listening socket closed");
            }
        }

        Ok(())
    }
}

// TODO Change error types.
async fn do_accept_worker<'c>(
    sock: &'c mut tnet::TcpListener,
    pm: &'c PeerManagerInner,
    handler: p2p::ConnHandler,
) -> Result<(), p2p::Error> {
    while let Some(item) = sock.next().await {
        let accepted = match item {
            Ok(i) => i,
            Err(e) => {
                // TODO Better handling.
                eprintln!("listen: {:?}", e);
                return Err(p2p::Error::NetworkFailure);
            }
        };

        let idx = pm.next_peer_idx.fetch_add(1, Ordering::Relaxed);

        let raddr = accepted.peer_addr().expect("get remote addr fail");
        eprintln!("[peerman] connection from {}, peer idx {}", raddr, idx);
        let pa = p2p::PeerAddr::TcpIp { sock: raddr };

        // This takes care of hooking up the IO workers, then we put it into ourself.
        let drop_sig = Arc::new(Notify::new());
        let dc_sig = Arc::new(Notify::new());
        let pc = init_peer(accepted, drop_sig.clone(), dc_sig.clone(), idx).await?;

        // Create a ticket and invoke the peer handler.
        let tkt = p2p::PeerTicketData::from_parts(pa, drop_sig, dc_sig, pc);
        handler(tkt).await;
    }

    Ok(())
}

/// After accepting the new connection, spawns off the worker to make it happen.
async fn init_peer(
    sock: tnet::TcpStream,
    stop_sig: Arc<Notify>,
    disconn_sig: Arc<Notify>,
    idx: u64,
) -> Result<Arc<PeerConnection>, p2p::Error> {
    let (out_tx, out_rx) = mpsc::channel(1); // change this?
    let (in_tx, in_rx) = mpsc::channel(32); // change this?

    let conn = PeerConnection {
        idx: idx,
        remote_addr: sock.peer_addr().expect("get remote addr fail"),
        packet_tx: out_tx,
        connected: AtomicBool::new(true),
        state: Mutex::new(PeerConnectionState::default()),
        stop_sig: stop_sig.clone(),
        packet_rx: Mutex::new(Box::new(in_rx) as p2p::PacketStreamInner),
    };
    let cref = Arc::new(conn);

    // TODO Do the encryption protocol handshake.
    // (sub) TODO Decide how to do encryption.

    // Spawn off the worker in another task to handle IO with this peer.
    // TODO Make this also do cleanup.
    let cref2 = cref.clone();
    let fut = async move {
        do_peer_io_worker(sock, out_rx, cref2, in_tx, stop_sig).await;
        disconn_sig.notify_waiters();
    };
    task::spawn(fut);

    Ok(cref)
}

/// Peer connection information.
pub struct PeerConnection {
    idx: u64,
    remote_addr: net::SocketAddr,

    packet_tx: mpsc::Sender<PktReq>,
    connected: AtomicBool,

    state: Mutex<PeerConnectionState>,
    stop_sig: Arc<Notify>,

    packet_rx: p2p::PacketStream,
}

pub type PeerConn = Arc<PeerConnection>;

#[derive(Default)]
struct PeerConnectionState {
    packets_sent: u64,
    bytes_written: u64,

    packets_recv: u64,
    bytes_read: u64,
}

#[async_trait]
impl p2p::PeerImpl for PeerConnection {
    fn get_ident(&self) -> Option<crypto::PubIdent> {
        // TODO
        None
    }

    fn is_connected(&self) -> bool {
        self.connected.load(Ordering::Relaxed)
    }

    async fn send_packet(&self, pkt: &p2p::Packet) -> Result<(), p2p::Error> {
        // FIXME This looks kinda gross.
        let (req, resp) = PktReq::new(pkt.clone());
        self.packet_tx
            .send(req)
            .map_err(|_| p2p::Error::NetworkFailure)
            .await?;
        resp.map_err(|_| p2p::Error::NetworkFailure).await?;
        Ok(())
    }

    async fn disconnect(&self) -> Result<(), p2p::Error> {
        // If it's currently false and we want to close it, do that.
        let test_res =
            self.connected
                .compare_exchange(true, false, Ordering::Relaxed, Ordering::Relaxed);

        eprintln!("called disconnect: {:?}", test_res);
        if let Err(true) = test_res {
            return Ok(());
        }

        // This sends all the other stuff into motion.
        self.stop_sig.notify_waiters();

        Ok(())
    }

    fn get_packet_stream<'p>(&'p self) -> Result<&'p p2p::PacketStream, p2p::Error> {
        if self.connected.load(Ordering::Relaxed) {
            Ok(&self.packet_rx)
        } else {
            Err(p2p::Error::PeerDisconnected)
        }
    }
}

// TODO Change error types.
async fn do_peer_read<'conn>(
    read_half: &'conn mut tnet::tcp::ReadHalf<'conn>,
    in_pkts: &mpsc::Sender<p2p::Packet>,
    conn: PeerConn,
) -> Result<(), p2p::Error> {
    let mut read_buf = BytesMut::new();

    loop {
        // TODO Add support for timeouts.
        match read_half.read_buf(&mut read_buf).await {
            // New data to read.
            Ok(n) => {
                if n == 0 {
                    eprintln!(
                        "[peerman] connection with {} closed unexpectedly",
                        conn.remote_addr,
                    );
                    break;
                }

                // Increment counters.
                let mut cs = conn.state.lock().await;
                cs.packets_recv += 1;
                cs.bytes_read += n as u64;

                match p2p::try_parse_frame(&read_buf) {
                    (Ok(pkt), n) => {
                        // Move the buffer up.
                        read_buf.advance(n);

                        // TODO Move this somewhere else.
                        if let p2p::Packet::Ping { msg } = &pkt {
                            // TODO Update the connection timeout thing.
                            match msg {
                                Some(msg) => {
                                    eprintln!("got ping from {}: {}", conn.remote_addr, msg);
                                }
                                None => {
                                    eprintln!("got ping from {} with no message", conn.remote_addr);
                                }
                            }
                            continue;
                        }

                        if let p2p::Packet::Disconnect = &pkt {
                            // TODO More info.
                            eprintln!("[peerman] disconnected from {}", conn.remote_addr);
                            break;
                        }

                        // Send the packet to the peer's incomming message
                        // queue.  If this fails then the other end was probably
                        // dropped and we exit.  If it blocks then there's a
                        // processing backup and this has the end effect of
                        // waiting before reading more bytes from the network.
                        if let Err(_) = in_pkts.send(pkt).await {
                            eprintln!("[peerman] warning: packet ingest failed, the other end may have been dropped");
                            // TODO Decide if we actually want to error.
                            break;
                        }
                    }
                    (Err(p2p::Error::IncompleteFrame), 0) => {
                        continue;
                    }
                    (Err(e), n) => {
                        eprintln!("error parsing packet: {:?}", e);
                        read_buf.advance(n);
                        continue;
                    }
                }
            }

            // Some other IO error.
            Err(e) => {
                eprintln!("[peerman] io error: {:?}", e);
                break;
            }
        }
    }

    Ok(())
}

// TODO Change error types.
async fn do_peer_write<'conn>(
    write_half: &'conn mut tnet::tcp::WriteHalf<'conn>,
    cmds: &mut mpsc::Receiver<PktReq>,
    conn: PeerConn,
) -> Result<(), p2p::Error> {
    let mut write_buf = Vec::new();

    loop {
        match cmds.recv().await {
            // New packet to send.
            Some(pkt_req) => {
                // Serialize the packet.
                // TODO Handle errors better.
                if let Err(err) = p2p::write_frame(&pkt_req.pkt, &mut write_buf) {
                    eprintln!("failed to serialize packet: {:?}", err);
                    pkt_req.done.send(Some(p2p::Error::NetworkFailure));
                    continue;
                }

                // Write the packet and clear the buffer.
                //eprintln!("sending packet {:?}", pkt_req.pkt);
                let written = match write_half.write_all(write_buf.as_slice()).await {
                    Ok(()) => {
                        let w = write_buf.len();
                        write_buf.clear();
                        w as u64
                    }
                    Err(e) => {
                        eprintln!("write packet: {:?}", e);
                        continue;
                    }
                };

                // Increment counters.
                let mut cs = conn.state.lock().await;
                cs.packets_sent += 1;
                cs.bytes_written += written;

                // Return.
                pkt_req.done.send(None);
            }

            // We want to close the connection.
            None => {
                eprintln!("[peerman] outbound packet channel closed");
                break;
            }
        }
    }

    Ok(())
}

/// Toplevel connection management future.  One spawned into a task for each
/// peer connection.  Does not handle re-connecting to a peer if the connection
/// fails.
async fn do_peer_io_worker(
    mut sock: tnet::TcpStream,
    mut cmds: mpsc::Receiver<PktReq>,
    conn: Arc<PeerConnection>,
    in_pkts: mpsc::Sender<p2p::Packet>,
    stop_sig: Arc<Notify>,
) {
    let (mut read_half, mut write_half) = sock.split();

    let read_fut = do_peer_read(&mut read_half, &in_pkts, conn.clone());
    let write_fut = do_peer_write(&mut write_half, &mut cmds, conn.clone());
    let stop_fut = stop_sig.notified();

    futures::pin_mut!(read_fut);
    futures::pin_mut!(write_fut);
    futures::pin_mut!(stop_fut);

    // TODO Set up keepalive worker.
    tokio::select! {
        res = read_fut => {
            match res {
                Ok(_) => {
                    eprintln!(
                        "[peerman] connection with {} was closed by remote",
                        conn.remote_addr
                    );
                },
                Err(e) => {
                    eprintln!(
                        "[peerman] connection with {} encountered error: {:?}",
                        conn.remote_addr,
                        e
                    );
                }
            }
        },
        _ = write_fut => {
            // TODO Do something more interesting.
            eprintln!(
                "[peerman] write task for {} exited unexpectedly",
                conn.remote_addr
            );
        },
        _ = stop_fut => {
            eprintln!("[peerman] closing connection with {}", conn.remote_addr);
        }
    }
}

#[derive(Clone)]
pub struct PeerCtl {
    pm_ref: PeerManager,
}

impl PeerCtl {
    pub fn new(pm: PeerManager) -> PeerCtl {
        PeerCtl { pm_ref: pm }
    }
}

#[async_trait]
impl p2p::PeerController for PeerCtl {
    async fn get_caps(&self) -> Vec<p2p::TransportCapability> {
        self.pm_ref.lock_inner().await.transport_caps.clone()
    }

    async fn connect(&self, addr: p2p::PeerAddr) -> Result<p2p::PeerTicket, p2p::Error> {
        match addr {
            p2p::PeerAddr::TcpIp { sock } => {
                let pmi = self.pm_ref.lock_inner().await;
                Ok(pmi.connect_tcp(sock).await?)
            }

            _ => Err(p2p::Error::NetworkFailure),
        }
    }

    async fn listen(
        &self,
        addr: p2p::PeerAddr,
        handler: p2p::ConnHandler,
    ) -> Result<p2p::ListenTicket, p2p::Error> {
        match addr {
            // TODO Make this deal with NATs properly.
            p2p::PeerAddr::TcpIp { sock } => {
                let notify = Arc::new(Notify::new());
                let tkt = p2p::ListenTicketData::from_notify(notify.clone());

                let pmi = self.pm_ref.lock_inner().await;
                pmi.listen(sock, notify, handler).await?;

                Ok(tkt)
            }

            _ => Err(p2p::Error::SpecifierUnsupported),
        }
    }

    /*
        async fn add_conn_directive(
            &self,
            dir: p2p::ConnDirective,
        ) -> Result<Option<p2p::Peer>, p2p::Error> {
            let mut lock = self.pc_data.lock().await;
            let tbl: &mut HashMap<u64, _> = &mut lock.directives_tbl;

            let notify = Arc::new(Notify::new());
            let dir_copy = dir.clone();

            if tbl.contains_key(&dir.id()) {
                return Err(p2p::Error::SpecifierConflict);
            }

            // TODO Actually spawn a watchdog to handle failures.
            let res = match dir.spec() {
                p2p::ConnSpecifer::ConnectTcp { ip, port } => {
                    let pmi = self.pm_ref.lock_inner().await;
                    let peer = pmi.connect_tcp(*ip, *port, notify.clone()).await?;
                    Some(peer)
                }
                p2p::ConnSpecifer::ListenTcp { ip, port } => {
                    let sa = net::SocketAddr::new(*ip, *port);
                    let pmi = self.pm_ref.lock_inner().await;
                    pmi.listen(sa, notify.clone()).await?;
                    None
                }
                _ => return Err(p2p::Error::SpecifierUnsupported),
            };

            tbl.insert(dir.id(), notify);
            Ok(res.map(p2p::generalize_peer_impl))
        }

        async fn del_conn_directive(&self, id: u64) -> Result<(), p2p::Error> {
            let mut lock = self.pc_data.lock().await;
            let tbl: &mut HashMap<u64, _> = &mut lock.directives_tbl;

            // Just pull out the thing and invoke the signal to stop it.
            if let Some(stop_sig) = tbl.remove(&id) {
                stop_sig.notify_waiters();
                Ok(())
            } else {
                Err(p2p::Error::SpecifierUnsupported)
            }
        }
    */
}

/// Wrapper struct for packets being sent to a peer, with a oneshot channel to
/// signal that it was sent successfully.
///
/// Not sure if this is the best way to do this.
struct PktReq {
    pkt: p2p::Packet,
    done: oneshot::Sender<Option<p2p::Error>>,
}

impl PktReq {
    fn new(pkt: p2p::Packet) -> (PktReq, oneshot::Receiver<Option<p2p::Error>>) {
        let (done, notif) = oneshot::channel();
        let pc = PktReq { pkt, done };
        (pc, notif)
    }
}
