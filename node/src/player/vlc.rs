//#![allow(unused)]

use std::collections::*;
use std::net;
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use std::sync::Arc;

use async_trait::async_trait;
use bytes::BytesMut;
use futures;
use futures::FutureExt;
use tokio::io::{AsyncWrite, AsyncWriteExt};
use tokio::net as tnet;
use tokio::process;
use tokio::sync::{mpsc, Mutex, Notify};

use proxcomm::client;
use proxcomm::stream;

const CHUNK_BUF_SIZE: usize = 4;

/// Stores some state needed for the VLC output.  May be refactored out.
pub struct PlayerHandle {
    buf_out: Mutex<mpsc::Receiver<Vec<u8>>>,
    exited_sig: Notify,
}

impl PlayerHandle {
    pub fn new() -> (PlayerHandle, mpsc::Sender<Vec<u8>>) {
        let (tx, rx) = mpsc::channel(CHUNK_BUF_SIZE);
        let ctl = PlayerHandle {
            buf_out: Mutex::new(rx),
            exited_sig: Notify::new(),
        };
        (ctl, tx)
    }

    async fn read_bytes_chunk(&self) -> Option<Vec<u8>> {
        let mut buf_lock = self.buf_out.lock().await;
        buf_lock.recv().await
    }

    pub async fn wait(&self) {
        self.exited_sig.notified().await
    }
}

pub async fn do_player_worker(conf: Config, ctl: Arc<PlayerHandle>) {
    // Open a socket for the player to connect to.
    let sa = net::SocketAddr::from(([127, 0, 0, 1], conf.local_port));
    let lis: tnet::TcpListener = tnet::TcpListener::bind(sa)
        .await
        .expect("bind playback port fail");
    let actual_port = if conf.local_port != 0 {
        conf.local_port
    } else {
        lis.local_addr()
            .expect("playback port local addr fail")
            .port()
    };

    // Spawn the child process so we can communicate with it.
    let mut cmd = process::Command::new(&conf.player_path);
    cmd.arg("--play-and-exit");
    cmd.arg(format!("tcp://127.0.0.1:{}", actual_port));
    if !conf.show_vlc {
        cmd.stderr(::std::process::Stdio::null());
    }

    let mut child = cmd.spawn().expect("spawn child player process fail");
    let child_id = child.id().expect("get child id fail");
    eprintln!("[output] Spawned child player process with id {}", child_id);

    // This is the main logic, in a block to make sure we close the connection
    // afterwards.
    {
        let (mut player_conn, _) = lis.accept().await.expect("accept player conn fail");
        eprintln!("[output] Got connection, starting feed!");

        // Now just keep waiting until we have bytes that we can do things with.
        let mut chunks_sent = 0;
        loop {
            if let Some(chunk) = ctl.read_bytes_chunk().await {
                let res = player_conn.write_all(&chunk.as_slice()).await;
                chunks_sent += 1;
                match res {
                    Ok(_) => {
                        eprintln!(
                            "[output] Wrote chunk {} of size {}",
                            chunks_sent,
                            chunk.len()
                        );
                    }
                    Err(e) => {
                        eprintln!("[output] Child write error {:?}", e);
                        break;
                    }
                }
            } else {
                eprintln!("[output] Chunk source closed");
                break;
            }
        }

        // Now flush the stream.
        player_conn.flush().await.expect("flush player conn fail");
        eprintln!("[output] Closed player connection");
    }

    // Wait for VLC to exit.
    eprintln!("[output] Waiting for VLC to exit...");
    if let Err(err) = child.wait().await {
        eprintln!(
            "[output] Failed to wait on child, leaving zombie: {:?}",
            err
        );
    } else {
        eprintln!("[output] Player exited");
    }

    // Now notify everyone waiting for it to finish.
    ctl.exited_sig.notify_waiters();
}

#[derive(Clone, Debug)]
pub struct Config {
    player_path: String,
    local_port: u16,

    /// If we want to show VLC output or drop it.
    show_vlc: bool,
}

impl Config {
    pub fn new(player_path: String, local_port: u16, show_vlc: bool) -> Config {
        Config {
            player_path,
            local_port,
            show_vlc,
        }
    }
}

/// Client output implementation that spawns an instance of VLC as a subprocess
/// and streams video chunk data to that over a local TCP connection.
///
/// When the stream is ended, the VLC instance will exit after playing to the
/// end of the stream.
///
/// TODO Make two flags for the state, one for if a stop was requested and
/// another when it's actually stopped.
pub struct VlcOutput {
    chan: Mutex<Option<mpsc::Sender<Vec<u8>>>>,
    running: AtomicBool,
    handle: Arc<PlayerHandle>,

    reordering: Mutex<ReorderState>,
}

struct ReorderState {
    chunks_in: u64,
    next_seq: u64,
    pending: HashMap<u64, stream::Chunk>,
}

impl VlcOutput {
    /// Starts a new VLC output viewer from a worker in a new task, returning a
    /// handle to interface with it.
    pub async fn start(conf: Config) -> VlcOutput {
        let (ph, tx) = PlayerHandle::new();
        let ph = Arc::new(ph);

        // Spawn the viewer to make it run.
        let out_fut = do_player_worker(conf, ph.clone());
        tokio::task::spawn(out_fut);

        let reord = ReorderState {
            chunks_in: 0,
            next_seq: 0,
            pending: HashMap::new(),
        };
        VlcOutput {
            chan: Mutex::new(Some(tx)),
            running: AtomicBool::new(true),
            handle: ph,
            reordering: Mutex::new(reord),
        }
    }
}

// Looks for more chunks in the reorder state that we can send to the output.
async fn process_next_seqs(state: &mut ReorderState, chan: &mut mpsc::Sender<Vec<u8>>) {
    while state.pending.contains_key(&state.next_seq) {
        let chunk = state.pending.remove(&state.next_seq).unwrap();
        state.next_seq += 1;
        chan.send(chunk.into_contents()).await;
    }
}

#[async_trait]
impl client::ClientOutputImpl for VlcOutput {
    fn is_active(&self) -> bool {
        self.running.load(Ordering::SeqCst)
    }

    async fn send_announcement(&self, msg: String) -> Result<(), client::Error> {
        eprintln!("[output] [ANN] {}", msg);
        Ok(())
    }

    async fn write_chunk(&self, chunk: stream::Chunk) -> Result<(), client::Error> {
        if !self.running.load(Ordering::SeqCst) {
            return Err(client::Error::DisplayStateError);
        }

        let mut state = self.reordering.lock().await;
        if state.chunks_in > 0 {
            if state.next_seq == chunk.seq_no() {
                state.next_seq += 1;

                // The Option in the Mutex is making the types weird.
                let mut chan_lock = self.chan.lock().await;
                let chan = chan_lock.as_mut().expect("feed chan closed prematurely");
                chan.send(chunk.into_contents()).await;

                // Since we might have advanced the head, we should try to look
                // for more we can send.
                process_next_seqs(&mut state, chan).await;
            } else {
                // Only insert it here, don't bother doing anything else.
                state.pending.insert(chunk.seq_no(), chunk);
            }
        } else {
            // Here we decide where the sequence numbers start.
            state.next_seq = chunk.seq_no() + 1;
            let mut chan_lock = self.chan.lock().await;
            chan_lock
                .as_mut()
                .expect("feed chan closed prematurely")
                .send(chunk.into_contents())
                .await;
        }

        state.chunks_in += 1;

        Ok(())
    }

    async fn end_stream(&self, wait: bool) -> Result<(), client::Error> {
        // If we were running, close the channel and stop.
        if self.running.fetch_and(false, Ordering::SeqCst) {
            let mut chan_opt = self.chan.lock().await;
            chan_opt.take();

            if wait {
                self.handle.wait().await;
            }
        }
        Ok(())
    }

    async fn wait_finished(&self) -> Result<(), client::Error> {
        // FIXME This seems like it might be able to race.
        if self.running.load(Ordering::SeqCst) {
            self.handle.wait().await;
            Ok(())
        } else {
            Err(client::Error::DisplayStateError)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use tokio::runtime as trt;
    use tokio::sync::mpsc;

    /// This just tests that process_next_seqs does what we expect it to.  It
    /// doesn't test the whole output thing, but that's the only part that's
    /// algorithmically interesting that we can test easily.
    ///
    /// We should always have some tests for the buffer management since that's
    /// pretty nontrivial.
    #[test]
    fn test_reorder_flush() {
        let (mut tx, mut rx) = mpsc::channel::<Vec<u8>>(32);
        let mut rx_ref = &mut rx;

        let chunks: Vec<u8> = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        let mut pending = chunks
            .iter()
            .map(|n| ((n + 10) as u64, stream::Chunk::new(0, *n as u64, vec![*n])))
            .collect();

        let mut reord = ReorderState {
            chunks_in: 0,
            next_seq: 11,
            pending: pending,
        };

        let mut output = Vec::new();
        let mut output_ref = &mut output;

        let proc_fut = async move {
            process_next_seqs(&mut reord, &mut tx).await;
            ::std::mem::drop(tx);
            while let Some(chunk) = rx_ref.recv().await {
                output_ref.push(chunk[0]);
            }
        };

        let mut rt = make_runtime();
        rt.block_on(proc_fut);

        assert_eq!(chunks, output);
    }

    fn make_runtime() -> trt::Runtime {
        trt::Builder::new_current_thread()
            .enable_all()
            .build()
            .expect("init runtime failure")
    }
}
