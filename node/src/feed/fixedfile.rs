#![allow(unused)]

use std::path::PathBuf;
use std::str::FromStr;
use std::time;

use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use serde_json;
use tokio::fs as tfs;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::process::Command;
use tokio::sync::{mpsc, Mutex};
use tokio::task;
use tokio::time as ttime;

use proxcomm::server;
use proxcomm::stream;

/// The size of the chunks we read out of the source file to send to ffmpeg.
/// It's 256 KiB as a decent size, but we should probably tune this.
const DEFAULT_SRC_BUF_SIZE: usize = 1 << 18;

#[derive(Debug)]
enum IntError {
    BadPath,
    ProbeFailure,
    ParseFailure,
}

#[derive(Clone, Debug)]
pub struct Config {
    /// Temp dir for storing output chunks.  Will be created if it doesn't
    /// already exist.
    transcode_dir: String,

    /// Duration of each chunk in seconds, let ffmpeg figure out what to do with
    /// it.  It ends up being larger than this usually.
    chunk_dur: u64,

    /// Invocation of the ffmpeg command, in case it's not `ffmpeg`.
    ffmpeg_cmd: String,

    /// Number of bytes to read from the source file at a time.
    source_buf_size: usize,

    /// Number of processed chunks waiting to be accepted by the destination.
    feed_buf_size: usize,

    /// If we want ffmpeg to inherit stderr or if we want to throw it away.
    show_ffmpeg: bool,

    /// Do we sleep ourselves when feeding chunks.
    feed_delay: bool,
}

impl Config {
    pub fn new_basic(transcode_dir: String) -> Config {
        Config {
            transcode_dir: transcode_dir,
            chunk_dur: 5,
            ffmpeg_cmd: String::from("ffmpeg"),
            source_buf_size: DEFAULT_SRC_BUF_SIZE,
            feed_buf_size: 4,
            show_ffmpeg: false,
            feed_delay: true,
        }
    }
}

pub struct FixedFileSource {
    state: Mutex<SrcState>,
    do_send_delay: bool,
}

struct SrcState {
    channel: mpsc::Receiver<stream::Chunk>,
    next_send_deadline: Option<time::Instant>,
}

impl FixedFileSource {
    pub async fn start(path: PathBuf, conf: Config) -> FixedFileSource {
        let feed_delay = conf.feed_delay;

        let (tx, rx) = mpsc::channel(conf.feed_buf_size);
        task::spawn(do_ffsrc_worker(path, conf, tx));

        let state = SrcState {
            channel: rx,
            next_send_deadline: None,
        };

        FixedFileSource {
            state: Mutex::new(state),
            do_send_delay: feed_delay,
        }
    }
}

#[async_trait]
impl server::VideoSource for FixedFileSource {
    async fn next_chunk(&self) -> Result<Option<stream::Chunk>, server::Error> {
        let mut st_lock = self.state.lock().await;

        if self.do_send_delay {
            // If we have a delay pending then wait on it.
            if let Some(gate_inst) = st_lock.next_send_deadline {
                ttime::sleep_until(ttime::Instant::from_std(gate_inst)).await;
            }

            // Get it, if it is something then set up the next deadline.
            let before = time::Instant::now();
            let res = st_lock.channel.recv().await;
            if let Some(chunk) = &res {
                let cdur = time::Duration::from_millis(chunk.duration());
                st_lock.next_send_deadline = Some(before + cdur);
            }

            Ok(res)
        } else {
            Ok(st_lock.channel.recv().await)
        }
    }
}

struct ChunkState {
    next_num: u64,
    next_path: String,
}

async fn do_process_new_chunks(
    st: &mut ChunkState,
    conf: &Config,
    dest: &mpsc::Sender<stream::Chunk>,
) -> bool {
    loop {
        match tfs::File::open(&st.next_path).await {
            Ok(mut chunk_file) => {
                // Read and send out the chunk.
                let mut buf: Vec<u8> = Vec::new();
                match chunk_file.read_to_end(&mut buf).await {
                    Ok(_) => {}
                    Err(e) => {
                        eprintln!("[videosrc] Error reading from chunk: {:?}", e);
                        return true;
                    }
                }

                // Also get the duration from here.
                let vpath = PathBuf::from(st.next_path.clone());
                let dur = match query_video_format(&vpath, "ffprobe".to_string()).await {
                    Ok(vfmt) => match f64::from_str(vfmt.duration.as_str()) {
                        Ok(d) => (d * 1000f64) as u64,
                        Err(e) => {
                            eprintln!(
                                "[videosrc] Duration of chunk from ffprobe failed to parse: {:?}",
                                e
                            );
                            return true;
                        }
                    },
                    Err(e) => {
                        eprintln!("[videosrc] Failed to probe chunk from thing");
                        return true;
                    }
                };

                let chunk = stream::Chunk::new(0, st.next_num, dur, buf);
                if let Err(_) = dest.send(chunk).await {
                    eprintln!("[videosrc] Feed destination closed, exiting");
                    return true;
                }

                eprintln!("[videosrc] Loaded chunk seq {}", st.next_num);

                // Delete the file.
                if let Err(e) = tfs::remove_file(&st.next_path).await {
                    eprintln!("[videosrc] [warning] Could not delete chunk: {:?}", e);
                }

                // Update our counters.
                st.next_num += 1;
                st.next_path = format!("{}/proxima_chunk_{}.ts", conf.transcode_dir, st.next_num);
            }
            // This just means we didn't give enough yet.
            Err(e) if e.kind() == ::std::io::ErrorKind::NotFound => {
                break;
            }
            // This means
            Err(e) => {
                eprintln!("[videosrc] Error opening next chunk");
                return true;
            }
        }
    }

    false
}

async fn do_ffsrc_worker(path: PathBuf, conf: Config, dest: mpsc::Sender<stream::Chunk>) {
    let mut pl_path = conf.transcode_dir.clone();
    pl_path.push_str("/proxima_pl.m3u8");

    // Open the source file.
    let mut src_file = match tfs::File::open(path).await {
        Ok(f) => f,
        Err(e) => {
            eprintln!("[videosrc] Error opening file: {:?}", e);
            return;
        }
    };

    // Spawn ffmpeg.
    let mut cmd = Command::new(&conf.ffmpeg_cmd);
    let filename_tmpl = format!("{}/proxima_chunk_%d.ts", conf.transcode_dir);
    #[rustfmt::skip]
    let args = vec![
        "-i", "-",
        "-vcodec", "copy",
        "-f", "hls",
        "-reset_timestamps", "0",
        "-hls_time", "2",
        "-hls_flags", "append_list+omit_endlist",
        "-hls_segment_filename", filename_tmpl.as_str(),
        pl_path.as_str(),
    ];
    cmd.args(args);
    cmd.stdin(::std::process::Stdio::piped());
    if !conf.show_ffmpeg {
        cmd.stderr(::std::process::Stdio::null());
    }

    let mut child = match cmd.spawn() {
        Ok(c) => c,
        Err(e) => {
            eprintln!("[videosrc] Error spawning transcoder: {:?}", e);
            return;
        }
    };

    let mut stdin = child.stdin.take().expect("get transcoder stdin fail");

    // Now continue to read sections off the disk to stuff into ffmpeg.
    let mut src_buf = vec![0; conf.source_buf_size];
    let mut chunk_state = ChunkState {
        next_num: 0,
        next_path: format!("{}/proxima_chunk_0.ts", conf.transcode_dir),
    };
    'srcfeed: loop {
        // Read from the source file.
        let bytes_read = match src_file.read_buf(&mut src_buf.as_mut_slice()).await {
            Ok(n) => {
                if n > 0 {
                    n
                } else {
                    eprintln!("[videosrc] Reached end of source video, cleaning up transcode workspace...");
                    break;
                }
            }
            Err(e) => {
                eprintln!("[videosrc] Error reading from source file: {:?}", e);
                break;
            }
        };

        // Stuff it into ffmpeg.
        match stdin.write_all(&src_buf[..bytes_read]).await {
            Ok(_) => {}
            Err(e) => {
                eprintln!("[videosrc] Error writing to transcoder: {:?}", e);
                break;
            }
        }

        // Try to open any new files, possibly exiting.
        if do_process_new_chunks(&mut chunk_state, &conf, &dest).await {
            break;
        }
    }

    // This closes the input, now we can wait for ffmpeg to finish.
    ::std::mem::drop(src_file);
    ::std::mem::drop(stdin);
    match child.wait().await {
        Ok(_) => {}
        Err(e) => {
            eprintln!("[videosrc] Error waiting on transcoder, ignoring: {:?}", e);
        }
    }

    // Detete the playlist file.  This is kinda jank.
    let pl_err = if let Err(e) = tfs::remove_file(&pl_path).await {
        eprintln!("[videosrc] Could not delete playlist file: {:?}", e);
        true
    } else {
        false
    };

    // Now read any remaining files.  Any errors here we can ignore.
    if do_process_new_chunks(&mut chunk_state, &conf, &dest).await || pl_err {
        eprintln!("[videosrc] [warning] Errors cleaning up transcode workspace, this may be a problem later.");
    }
}

async fn query_video_format(path: &PathBuf, ffprobe_cmd: String) -> Result<Format, IntError> {
    // Call out to ffprobe to make it work.
    // TODO Make this configurable.
    let mut cmd = Command::new(ffprobe_cmd);
    let args = vec![
        "-v",
        "quiet",
        "-hide_banner",
        "-show_format",
        "-print_format",
        "json",
    ];
    cmd.args(args);
    cmd.arg(path.as_os_str());
    cmd.stderr(::std::process::Stdio::null());
    let out = cmd.output().await.map_err(|_| IntError::ProbeFailure)?;

    // Then just parse it and return the inner value.
    serde_json::from_slice::<ProbeOutput>(out.stdout.as_slice())
        .map(|v| v.format)
        .map_err(|_| IntError::ParseFailure)
}

#[derive(Debug, Serialize, Deserialize)]
struct ProbeOutput {
    format: Format,
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default)]
struct Format {
    filename: String,
    nb_streams: u32,
    nb_programs: u32,
    format_name: String,
    format_long_name: String,
    start_time: String,
    duration: String,
    size: String,
    bit_rate: String,
    probe_score: u16,
}
