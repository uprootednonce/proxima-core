#![allow(unused)]
#![feature(shrink_to)]

use std::net;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::time;

use argh::FromArgs;

use tokio::fs as tfs;
use tokio::io::{AsyncWrite, AsyncWriteExt};
use tokio::prelude::*;
use tokio::runtime as trt;
use tokio::sync;
use tokio::task;
use tokio::time as ttime;

use proxcomm::crypto;
use proxcomm::p2p::{self, PeerImpl};

mod feed;
mod peerman;
mod player;

#[derive(FromArgs, PartialEq, Debug)]
#[argh(description = "proxima node")]
struct Args {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Subcommand {
    TestListen(SubcListen),
    TestConnect(SubcConnect),
    TestSimplePlay(SubcSimplePlay),
    TestChunkedPlay(SubcChunkedPlay),
    TestSimpleCast(SubcSimpleCast),
    TestSimpleView(SubcSimpleView),
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "test-listen", description = "tests p2p code")]
struct SubcListen {
    #[argh(
        option,
        description = "port (default: 4269)",
        short = 'p',
        default = "4269"
    )]
    port: u16,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "test-connect", description = "tests p2p code")]
struct SubcConnect {
    #[argh(positional)]
    addr: net::IpAddr,

    #[argh(positional, default = "4269")]
    port: u16,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "test-simple-play",
    description = "tests playback logic"
)]
struct SubcSimplePlay {
    #[argh(positional)]
    video: PathBuf,

    #[argh(
        option,
        description = "VLC path",
        short = 'p',
        default = "get_system_vlc_path()"
    )]
    player: String,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "test-chunked-play",
    description = "tests playback logic more realistically"
)]
struct SubcChunkedPlay {
    #[argh(positional)]
    video: PathBuf,

    #[argh(
        option,
        description = "VLC path",
        short = 'p',
        default = "get_system_vlc_path()"
    )]
    player: String,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "test-simple-cast",
    description = "tests casting logic"
)]
struct SubcSimpleCast {
    #[argh(positional)]
    desc_path: PathBuf,

    #[argh(positional)]
    video: PathBuf,

    #[argh(positional, default = "4269")]
    port: u16,

    #[argh(option, description = "transcode workdir path", short = 'w')]
    workdir: Option<String>,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "test-simple-view",
    description = "tests viewership logic"
)]
struct SubcSimpleView {
    #[argh(positional)]
    desc_path: PathBuf,

    #[argh(positional)]
    addr: net::IpAddr,

    #[argh(positional, default = "4269")]
    port: u16,

    #[argh(
        option,
        description = "VLC path",
        short = 'p',
        default = "get_system_vlc_path()"
    )]
    player: String,
}

fn main() {
    use Subcommand::*;

    let args: Args = argh::from_env();

    let ident = crypto::Identity::rand();
    let pc = peerman::PeeringConfig::from_ident(&ident);

    let mut pm = peerman::PeerManager::new(pc);

    let mut rt: trt::Runtime = make_runtime();

    let main_fut = async {
        match args.subcommand {
            TestListen(cmd) => main_listen(cmd, pm).await,
            TestConnect(cmd) => main_connect(cmd, pm).await,
            TestSimplePlay(cmd) => main_simple_play(cmd).await,
            TestChunkedPlay(cmd) => main_chunked_play(cmd).await,
            TestSimpleCast(cmd) => main_simple_cast(cmd, pm).await,
            TestSimpleView(cmd) => main_simple_view(cmd, pm).await,
        }
    };

    rt.block_on(main_fut);
}

async fn main_listen(cmd: SubcListen, pm: peerman::PeerManager) {
    let sa = net::SocketAddr::from(([0, 0, 0, 0], cmd.port));
    let pmi = pm.lock_inner().await;
    let notify = Arc::new(sync::Notify::new());

    let mut tickets = ::std::sync::Mutex::new(Vec::new());
    let handler = move |tkt: p2p::PeerTicket| {
        eprintln!("doing something with the connection from {:?}", tkt.addr());

        // Technically this is blocking but that's ok it's just for testing.
        let mut lock = tickets.lock().expect("tickets fail");
        lock.push(tkt);
    };
    let hbox: p2p::ConnHandler = p2p::make_basic_conn_handler(handler);

    pmi.listen(sa, notify, hbox).await.expect("listen fail");
}

async fn main_connect(cmd: SubcConnect, pm: peerman::PeerManager) {
    let sock = net::SocketAddr::from((cmd.addr, cmd.port));
    let pmi = pm.lock_inner().await;
    let tkt: p2p::PeerTicket = pmi.connect_tcp(sock).await.expect("connect fail");
    eprintln!("connected!");

    let mut i = 0u64;
    loop {
        // Just send a ping.
        let pkt = p2p::Packet::Ping {
            msg: Some(format!("hello {}!", i)),
        };
        i += 1;
        println!("sending ping {}", i);
        let peer = tkt.get_peer().await.expect("get peer fail");
        peer.send_packet(&pkt).await.expect("send ping fail");

        // Sleep a bit.
        tokio::time::sleep(time::Duration::from_secs(1)).await;

        if i == 10 {
            break;
        }
    }

    // FIXME
    //conn.disconnect();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
}

const SIMPLE_PLAY_BUF_SIZE: usize = 0x100000;

async fn main_simple_play(cmd: SubcSimplePlay) {
    let conf = player::vlc::Config::new(cmd.player, 0, true);

    let (ph, tx) = player::vlc::PlayerHandle::new();
    let ph = Arc::new(ph);

    let video = cmd.video.clone();
    let ph2 = ph.clone();
    let read_fut = async move {
        eprintln!("[main] opening source file: {:?}", video);
        let mut file = tfs::File::open(&video).await.expect("open video fail");

        // Read chunks of the file and stuff them into the channel.
        let mut i = 0;
        loop {
            // Ok to make a new one, we move it after reading into it.
            let mut read_buf = vec![0; SIMPLE_PLAY_BUF_SIZE];

            match file.read_buf(&mut read_buf.as_mut_slice()).await {
                Ok(n) => {
                    i += 1;
                    if n > 0 {
                        read_buf.truncate(n);
                        tx.send(read_buf).await;
                    } else {
                        break;
                    }
                }
                Err(e) => {
                    eprintln!("[main] error reading bytes from source: {:?}", e);
                    break;
                }
            }
        }

        // Close the channel.
        eprintln!("[main] finished reading source file, closing feed");
        ph2.wait().await;
    };

    let play_fut = player::vlc::do_player_worker(conf, ph);
    futures::join!(read_fut, play_fut);
}

async fn main_chunked_play(cmd: SubcChunkedPlay) {
    use feed::fixedfile;
    use proxcomm::client::ClientOutputImpl;
    use proxcomm::server::VideoSource;

    let src_conf = fixedfile::Config::new_basic(".".to_string());
    let vlc_conf = player::vlc::Config::new(cmd.player, 0, true);

    let src = fixedfile::FixedFileSource::start(cmd.video.clone(), src_conf).await;
    let out = player::vlc::VlcOutput::start(vlc_conf).await;

    while let Some(chunk) = src.next_chunk().await.expect("get chunk fail") {
        let dur = chunk.duration();
        eprintln!("[main] Relaying chunk {}", chunk.seq_no());
        out.write_chunk(chunk).await.expect("play chunk fail");
        ttime::sleep(ttime::Duration::from_millis(dur - 1)).await;
    }
    out.end_stream(true).await;

    eprintln!("[main] Done!");
}

async fn main_simple_cast(cmd: SubcSimpleCast, pm: peerman::PeerManager) {
    eprintln!("[main] Getting ready to start stream server from fixed file...");

    // Make all the keys!
    let ident = crypto::Identity::rand();
    let pub_ident = ident.to_pub();
    let (author_pk, author_sk) = crypto::Prvkey::rand_pair();
    let (stream_pk, stream_sk) = crypto::Prvkey::rand_pair();
    let (enc_desc, dec_desc) = proxcomm::encdec::make_desc_pair(author_sk, stream_sk);

    // Set up networking.
    let lis_ip = net::IpAddr::from([0, 0, 0, 0]);
    let lis_addr = p2p::PeerAddr::TcpIp {
        sock: net::SocketAddr::from((lis_ip, cmd.port)),
    };

    // TODO Make this do smarter things with UPnP and stuff.
    let ext_ip = net::IpAddr::from([127, 0, 0, 1]);
    let ext_addr = p2p::PeerAddr::TcpIp {
        sock: net::SocketAddr::from((ext_ip, cmd.port)),
    };

    // Set up the feed.
    let ffconf =
        feed::fixedfile::Config::new_basic(cmd.workdir.clone().unwrap_or_else(|| ".".to_string()));
    let ffsrc = feed::fixedfile::FixedFileSource::start(cmd.video.clone(), ffconf).await;
    let ffsrc = Box::new(ffsrc) as Box<dyn proxcomm::server::VideoSource>;

    // Launch the server and start the stream.
    let pctl = Arc::new(peerman::PeerCtl::new(pm)) as p2p::PeerCtl;
    eprintln!("[main] Starting server and stream...");
    let server_handle = proxcomm::server::Server::start(ident, pctl, lis_addr).await;
    let sid = match server_handle.start_stream(ffsrc, enc_desc).await {
        Ok(sid) => sid,
        Err(e) => {
            eprintln!("[main] Could not start stream: {:?}", e);
            return;
        }
    };

    // Write the desc file.
    let stream_desc = proxcomm::discovery::StreamDesc::new(sid, pub_ident, ext_addr, dec_desc);
    let sdesc_buf = serde_json::to_vec(&stream_desc).expect("serialize sdesc fail");
    {
        let mut sdesc_file = tfs::File::create(&cmd.desc_path)
            .await
            .expect("open sdesc file fail");
        sdesc_file
            .write_all(&sdesc_buf)
            .await
            .expect("write sdesc file fail");
    }

    // Wait for it to finish.
    server_handle
        .wait_for_stream(sid)
        .await
        .expect("stream wait");
}

async fn main_simple_view(cmd: SubcSimpleView, pm: peerman::PeerManager) {
    // Open and read the stream desc file.
    let desc = {
        let mut desc_file = tfs::File::open(cmd.desc_path)
            .await
            .expect("open desc file fail");
        let mut buf = Vec::new();
        desc_file
            .read_to_end(&mut buf)
            .await
            .expect("read desc file fail");
        serde_json::from_slice::<proxcomm::discovery::StreamDesc>(buf.as_slice())
            .expect("deserialize desc file fail")
    };

    // Make the key.
    let client_ident = crypto::Identity::rand();

    // Set up the video output.
    let vlc_conf = player::vlc::Config::new(cmd.player, 0, true);
    let vlc_out = player::vlc::VlcOutput::start(vlc_conf).await;
    let output = Box::new(vlc_out) as proxcomm::client::ClientOutput;

    // Now launch the client!
    let pctl = Arc::new(peerman::PeerCtl::new(pm)) as p2p::PeerCtl;
    let client = proxcomm::client::Client::start(client_ident, desc, pctl, output)
        .await
        .expect("start client fail");
    client.wait().await.expect("wait fail");
}

fn make_runtime() -> trt::Runtime {
    trt::Builder::new_multi_thread()
        .enable_all()
        .thread_name("proxima-worker")
        .build()
        .expect("init runtime failure")
}

/// Utility function to return the path to the player on this system.
///
/// On Linux this just runs `vlc` reading from stdin, but on Windows we need to
/// figure out the actual path to it.
// TODO Implement this for Windows, macOS.
fn get_system_vlc_path() -> String {
    "vlc".to_string()
}
