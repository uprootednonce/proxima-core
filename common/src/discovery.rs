use serde_derive::{Deserialize, Serialize};

use crate::crypto;
use crate::encdec;
use crate::p2p;
use crate::stream;

/// Description that clients need to find a stream and begin receiving content.
///
/// These settings are assumed to be trusted, we may want to make a version of
/// this that's signed by the caster's ident that can be verified by clients.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct StreamDesc {
    stream_id: stream::StreamId,
    caster_ident: crypto::PubIdent,
    caster_addr: p2p::PeerAddr,
    decoder_params: encdec::StreamDecoderDesc,
}

impl StreamDesc {
    pub fn new(
        stream_id: stream::StreamId,
        caster_ident: crypto::PubIdent,
        caster_addr: p2p::PeerAddr,
        decoder_params: encdec::StreamDecoderDesc,
    ) -> StreamDesc {
        StreamDesc {
            stream_id,
            caster_ident,
            caster_addr,
            decoder_params,
        }
    }

    pub fn stream_id(&self) -> stream::StreamId {
        self.stream_id
    }

    pub fn caster_ident(&self) -> crypto::PubIdent {
        self.caster_ident
    }

    pub fn caster_addr(&self) -> &p2p::PeerAddr {
        &self.caster_addr
    }

    pub fn make_decoder(&self) -> encdec::Decoder {
        encdec::Decoder::new(self.stream_id, self.decoder_params.clone())
    }
}
