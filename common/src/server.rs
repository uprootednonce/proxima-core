#![allow(unused)]

use std::collections::*;
use std::future::Future;
use std::sync::Arc;

use async_trait::async_trait;
use futures::StreamExt;
use tokio::sync::{mpsc, Mutex, Notify, RwLock};
use tokio::task;

use crate::crypto;
use crate::encdec;
use crate::p2p;
use crate::router;
use crate::stream;

/// How many packets should we allow to be queued in processing for a stream manager.
const SM_PACKET_BUF_SIZE: usize = 32;

#[derive(Debug)]
pub enum Error {
    /// Could not read a new chunk from the input soon enough.
    FeedTimeout,

    /// If a remote peer sends a request that is is invalid, such that they
    /// should not have sent it if they were properly following the protocol and
    /// with correct information.
    InvalidRequest,

    /// If we tried waiting on a stream that doesn't exist.
    InvalidStream,

    /// Any error with encoding a bundle.
    Encoder(encdec::Error),
}

/// High level client for watching a particular stream.
pub struct Server {
    /// Static user identity.
    user_id: crypto::Identity,

    /// Peer control interface.
    peer_ctl: p2p::PeerCtl,

    /// Table of what streams this server is currently authoring.
    streams: RwLock<HashMap<stream::StreamId, Arc<StreamManager>>>,

    /// Table of inputs to stream managers.
    /// TODO Should we move this?
    stream_pkts: RwLock<HashMap<stream::StreamId, Arc<mpsc::Sender<(p2p::Peer, p2p::Packet)>>>>,

    /// Signal that the server is shutting down.
    stop_sig: Notify,
}

impl Server {
    /// Starts a new server listening on the given address.
    pub async fn start(
        ident: crypto::Identity,
        peer_ctl: p2p::PeerCtl,
        listen_addr: p2p::PeerAddr,
    ) -> Arc<Server> {
        let serv = Server {
            user_id: ident,
            peer_ctl: peer_ctl,
            streams: RwLock::new(HashMap::new()),
            stream_pkts: RwLock::new(HashMap::new()),
            stop_sig: Notify::new(),
        };
        let serv = Arc::new(serv);

        // Spawn the listening task.
        task::spawn(do_server_listen_worker(serv.clone(), listen_addr));

        serv
    }

    pub async fn start_stream(
        self: &Arc<Server>,
        source: Box<dyn VideoSource>,
        enc_desc: encdec::StreamEncoderDesc,
    ) -> Result<stream::StreamId, Error> {
        let sid = stream::StreamId::new_rand();
        let enc = encdec::Encoder::new(sid, enc_desc);
        let (rh, route_tx) = router::RouterHandle::start().await;

        let sm = StreamManager {
            stream_id: sid,
            bundle_encoder: Mutex::new(enc),
            vid_source: source,
            route_input: route_tx,
            local_router_handle: rh,
            finish_sig: Notify::new(),
        };
        let man = Arc::new(sm);

        let (sm_tx, sm_rx) = mpsc::channel::<(p2p::Peer, p2p::Packet)>(SM_PACKET_BUF_SIZE);
        let sm_tx = Arc::new(sm_tx);

        // Add the stream to the map.
        {
            let mut s_lock = self.streams.write().await;
            s_lock.insert(sid, man.clone());
        }
        // Add the input channel to the map.
        {
            let mut sp_lock = self.stream_pkts.write().await;
            sp_lock.insert(sid, sm_tx);
        }

        // Start the workers for the stream.
        task::spawn(do_stream_feed_worker(man.clone()));
        task::spawn(do_stream_man_worker(self.clone(), man, sm_rx));

        Ok(sid)
    }

    pub async fn wait_for_stream(
        self: &Arc<Server>,
        stream_id: stream::StreamId,
    ) -> Result<(), Error> {
        let stream = {
            let streams = self.streams.read().await;
            match streams.get(&stream_id) {
                Some(s) => s.clone(),
                None => return Err(Error::InvalidStream),
            }
        };

        stream.finish_sig.notified().await;
        Ok(())
    }
}

/// Control object for a single stream.
pub struct StreamManager {
    stream_id: stream::StreamId,

    /// Encoder used to make bundles of chunks and other messages.
    bundle_encoder: Mutex<encdec::Encoder>,

    /// Host's video source to propagate out to viewers.
    vid_source: Box<dyn VideoSource>,

    /// Input for bundles into the router task to be routed.
    route_input: mpsc::Sender<router::RoutingRequest>,

    /// Handle to update our local routing decisions.
    local_router_handle: router::RouterHandle,

    /// Signal sent off when the stream finishes.
    finish_sig: Notify,
}

/// Video stream coming from whatever's giving us the video.
#[async_trait]
pub trait VideoSource
where
    Self: Sync + Send,
{
    /// Read the next chunk from the video source.  These should always have
    /// increasing sequence numbers
    ///
    /// If this is none, then we're at the end of the stream.
    async fn next_chunk(&self) -> Result<Option<stream::Chunk>, Error>;
}

/// Takes a chunk and encodes it into a packed bundle instructing downstreams to
/// play back the chunk.
async fn make_chunk_bundle(
    enc: &mut encdec::Encoder,
    chunk: stream::Chunk,
) -> Result<encdec::PackedBundle, Error> {
    let cmd = stream::Command::Msg(stream::Message::Chunk(chunk));
    let breq = encdec::BundleRequest::new(vec![cmd]);
    enc.create_packed_bundle(breq).map_err(Error::Encoder)
}

/// Processes chunks coming from the source stream and encodes them into bundles
/// to be routed.
async fn do_stream_feed_worker(state: Arc<StreamManager>) -> Result<(), Error> {
    // Just keep trying to read and doing things then.
    loop {
        match state.vid_source.next_chunk().await {
            Ok(Some(next)) => {
                let seq = next.seq_no();
                let mut enc = state.bundle_encoder.lock().await;
                let bundle = make_chunk_bundle(&mut enc, next).await?;
                let rreq = router::RoutingRequest::New(bundle);
                eprintln!("[streamserver] Routing chunk {}", seq);
                match state.route_input.send(rreq).await {
                    Ok(_) => {}
                    Err(e) => {
                        break;
                    }
                }
            }
            Ok(None) => {
                break;
            }
            Err(e) => {
                eprintln!("[streamserver] Error receiving chunk from source: {:?}", e);
            }
        }
    }

    // TODO Send EndStream message.
    state.finish_sig.notify_waiters();
    eprintln!("[streamserver] End of stream");

    Ok(())
}

/// Main task for handling relevant packets from peers.
async fn do_stream_man_worker(
    serv: Arc<Server>,
    man: Arc<StreamManager>,
    mut pkts: mpsc::Receiver<(p2p::Peer, p2p::Packet)>,
) {
    loop {
        let recv_fut = pkts.recv();
        futures::pin_mut!(recv_fut);

        if let Some((peer, pkt)) = recv_fut.await {
            match pkt {
                p2p::Packet::SubUpdate(sub) => {
                    if let Err(e) = process_sub_update(&sub, &peer, serv.as_ref()).await {
                        // TODO Make this more clear.
                        eprintln!("[streamserver] Failed to update a peer's subscription state");
                    }
                }
                _ => {
                    // TODO
                }
            }
        }
    }

    // TODO Make this Display and print it as hex.
    eprintln!("[streamserver] cleaning up stream {}", man.stream_id);
}

/// Processes a request to update a a peer's subscription state on a stream.
/// Later this should be spawned as its own task.
///
/// FIXME Do some throttling on this since a user could partially DoS us by
/// forcing us to keep acquiring this lock by repeatedly sending update packets.
///
/// TODO Eventually make this go through a more advanced graph-based routing
/// engine that has the nice properties we want.
async fn process_sub_update(
    state: &p2p::SubState,
    peer: &p2p::Peer,
    srv: &Server,
) -> Result<(), Error> {
    let mut streams = srv.streams.write().await;

    // Find the stream manager for this stream.
    if let Some(mut sman) = streams.get_mut(&state.stream_id) {
        let rh = &sman.local_router_handle;

        // If true then add it if it doesn't exist or remove it if it does.
        // TODO Be more detailed.
        if state.state {
            eprintln!("[streamserver] Subscription for peer added");
            rh.add_downstream(peer).await;
        } else {
            eprintln!("[streamserver] Subscription for peer removed");
            rh.remove_downstream(peer).await;
        }

        Ok(())
    } else {
        Err(Error::InvalidRequest)
    }
}

/// Main processing logic for handling packets from users.
async fn handle_peer_packet(
    pkt: &p2p::Packet,
    peer: &p2p::Peer,
    srv: &Server,
) -> Result<(), Error> {
    match pkt {
        p2p::Packet::SubUpdate(sub) => {
            let streams_lock = srv.stream_pkts.read().await;
            if let Some(inp) = streams_lock.get(&sub.stream_id) {
                inp.send((peer.clone(), pkt.clone())).await;
            } else {
                eprintln!("[streamserver] Packet had invalid stream ID, dropping");
            }
        }
        _ => {
            // TODO
            eprintln!("[streamserver] ignoring packet from peer");
        }
    }

    Ok(())
}

/// Invoked when a peer disconnects, to perform cleanup operations like
/// unregistering them from various things.
async fn do_peer_cleanup(serv: Arc<Server>, peer: &p2p::Peer) -> Result<(), Error> {
    // Unregister them from any subscriptions.
    // FIXME Make it so we don't have to lock all the stream managers.
    {
        let mans_lock = serv.streams.read().await;
        for (sid, sman) in mans_lock.iter() {
            if sman.local_router_handle.remove_downstream(peer).await {
                // TODO Say the peer in here.
                eprintln!("[streamserver] Unsubscribed peer from stream {}", sid);
            }
        }
    }

    Ok(())
}

async fn do_peer_monitor_worker(serv: Arc<Server>, tkt: p2p::PeerTicket) {
    if let Ok(peer) = tkt.get_peer().await {
        // Just take each packet and dispatch it to the packet handler.
        loop {
            let stream_mtx = match peer.get_packet_stream() {
                Ok(sm) => sm,
                Err(_) => continue,
            };
            let mut stream_lock = stream_mtx.lock().await;
            let mut stream = stream_lock.as_mut();

            let recv_fut = stream.next();
            futures::pin_mut!(recv_fut);

            if let Some(pkt) = recv_fut.await {
                match handle_peer_packet(&pkt, &peer, serv.as_ref()).await {
                    Ok(()) => {}
                    Err(e) => {
                        eprintln!(
                            "[streamserver] error processing packet from {}: {:?}",
                            tkt.addr(),
                            e
                        );
                    }
                }
            } else {
                break;
            }
        }

        // Now unregister it from things now that we're done.
        eprintln!("[streamserver] Disconnected: {}", tkt.addr());
        if let Err(e) = do_peer_cleanup(serv, &peer).await {
            // TODO Say the peer in here.
            eprintln!(
                "[streamserver] Error cleaning up peer {}: {:?}",
                tkt.addr(),
                e
            );
        }
    }
}

async fn do_server_listen_worker(serv: Arc<Server>, addr: p2p::PeerAddr) {
    // All this cloning because the lifetimes are screwey.
    // FIXME Actually I don't think this needs to be as fancy.
    let sa = serv.clone();
    let handler = move |tkt: p2p::PeerTicket| {
        let sa2 = sa.clone();
        let fut = async move {
            // Create the task to process peer packets.
            task::spawn(do_peer_monitor_worker(sa2, tkt));
        };
        Box::pin(fut) as ::std::pin::Pin<Box<dyn Future<Output = ()> + Send>>
    };
    let hbox = Box::new(handler) as p2p::ConnHandler;

    eprintln!("[streamserver] Starting listen on {}", addr);
    let lis_handle = serv.as_ref().peer_ctl.as_ref().listen(addr, hbox).await;

    // Now wait for us to stop.  We're just a watchdog for the other task.
    serv.as_ref().stop_sig.notified().await;
}
