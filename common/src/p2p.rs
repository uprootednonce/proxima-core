#![allow(unused)]

use std::fmt::{self, Debug, Display};
use std::net;
use std::ops::Deref;
use std::sync::Arc;

use async_trait::async_trait;
use futures::future::BoxFuture;
use futures::prelude::*;
use rmp_serde;
use serde_derive::{Deserialize, Serialize};
use tokio::sync::{Mutex, Notify};

use crate::crypto;
use crate::encdec;
use crate::stream;

/// Fixed maximum frame size.  This number includes the length prefix, so the
/// max length of the body is 4 bytes less than this number.
const MAX_FRAME_SIZE: usize = (1 << 24); // 16 MiB, keep under 4 GiB.

#[derive(Debug)]
pub enum Error {
    /// Tried to send a packet while disconnected or the connection was already
    /// closed.
    PeerDisconnected,

    /// When trying to connect, the connection directive was invalid.
    InvalidEndpoint,

    /// Usually an error deserializing, but can be an error serializing.
    InvalidPacket,

    /// There wasn't enough bytes provided to read the full packet frame.
    IncompleteFrame,

    /// Encountered a frame bigger than the cap.
    OversizedFrame,

    /// Some lower level network error.
    NetworkFailure,

    /// If there's some conflict with specifiers, like if we're told to listen
    /// on the same port multiple times or if the ID is already in use.
    SpecifierConflict,

    /// If we tried to use a specifier of an unsupported type, or we tried to
    /// refer to a specifier with an invalid ID.
    SpecifierUnsupported,
}

/// P2P wire layer packet.  Transport encryption is not handled here.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase", tag = "mty")]
pub enum Packet {
    /// Sends a ping, with an optional message.
    ///
    /// NB: This should not be passed to the application layer.
    Ping { msg: Option<String> },

    /// Explicit, graceful disconnection.
    ///
    /// NB: This should not be passed up to the application layer.
    Disconnect,

    /// Sending a content bundle to the other peer, does not need to be
    /// explicitly requested.
    PushBundle(encdec::PackedBundle),

    /// Request for a set of bundles to be relayed, if available.
    ReqBundles(BundleSet),

    /// Update from a viewer to a caster about a subscription to a thing.
    SubUpdate(SubState),
}

/// A list of bundles, for whatever purpose.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct BundleSet {
    pub bids: Vec<u64>,
}

#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct SubState {
    pub stream_id: stream::StreamId,
    pub state: bool,
}

/// Tries to parse a packet frame from the buffer, returning the packet.  In
/// either case, also returns the number of bytes read to faciliate correct
/// buffer management.
pub fn try_parse_frame(buf: &[u8]) -> (Result<Packet, Error>, usize) {
    if buf.len() < 4 {
        return (Err(Error::IncompleteFrame), 0);
    }

    // Frame lengths are always big endian.
    let lenbuf = [buf[0], buf[1], buf[2], buf[3]];
    let body_len = u32::from_be_bytes(lenbuf) as usize;
    let frame_len = body_len + 4;

    if frame_len > MAX_FRAME_SIZE {
        // TODO Is this the correct thing to do?
        return (Err(Error::OversizedFrame), 4);
    }

    if buf.len() < frame_len {
        return (Err(Error::IncompleteFrame), 0);
    }

    // Now that we know how long it is, just parse it and return the final value.
    let body_buf = &buf[4..frame_len];
    let res = rmp_serde::from_slice(body_buf).map_err(|_| Error::InvalidPacket);
    (res, frame_len)
}

/// Writes the packet to the buffer, extending it from where it is now to permit
/// holding multiple frames in a single buffer.
///
/// Will return an error without writing to the buffer if serialization fails
/// for any reason or if the generated packet is oversized.
pub fn write_frame(pkt: &Packet, buf: &mut Vec<u8>) -> Result<(), Error> {
    let tmpbuf = rmp_serde::to_vec(pkt).map_err(|_| Error::InvalidPacket)?;
    let frame_len = tmpbuf.len() + 4;
    if frame_len > MAX_FRAME_SIZE {
        return Err(Error::OversizedFrame);
    }

    buf.reserve(buf.len() + frame_len);
    let lenbuf = (tmpbuf.len() as u32).to_be_bytes(); // this is checked
    buf.extend_from_slice(&lenbuf[..]);
    buf.extend_from_slice(tmpbuf.as_slice());
    Ok(())
}

/// Transport capabilites are used specify the different methods a node can
/// communicate with other nodes.  This does not specify the flow of bundles
/// through the graph, just the type of connection and who initiates where
/// applicable.
///
/// TODO Eventually add UDP and WebRTC unreliable transports to this.
#[derive(Clone, Hash, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase", tag = "ty")]
pub enum TransportCapability {
    /// Outbound TCP connection.
    TcpOutbound,

    /// Accept inbound TCP connections, either being not behind a firewall or
    /// with proper routing through it.  If port is 0, then any port.
    TcpInbound { ip: net::IpAddr, port: u16 },

    /// WebSockets outbound, such as in a browser.
    WsOutbound,

    /// WebSockets inbound, similarly to TCP inbound, but for working with
    /// clients that only have the above.
    WsInbound { url: String },
}

/// Generic address a peer can be connected to at.
#[derive(Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase", tag = "ty")]
pub enum PeerAddr {
    /// Normal peer over TCP/IP, is most general.
    TcpIp { sock: net::SocketAddr },

    /// Web server accepting connections over WebSockets.
    Ws { url: String },

    /// Browser connecting to us over WebSockets.
    Browser { sock: net::SocketAddr },
}

impl PeerAddr {
    pub fn to_connect_spec(&self) -> Result<ConnSpecifer, Error> {
        let v = match self.clone() {
            PeerAddr::TcpIp { sock } => ConnSpecifer::ConnectTcp { sock },
            PeerAddr::Ws { url } => ConnSpecifer::ConnectWs { url },
            _ => return Err(Error::InvalidEndpoint),
        };
        Ok(v)
    }

    pub fn to_listen_spec(&self) -> Result<ConnSpecifer, Error> {
        let v = match self.clone() {
            PeerAddr::TcpIp { sock } => ConnSpecifer::ListenTcp { sock },
            PeerAddr::Ws { url } => ConnSpecifer::ListenWs { url },
            _ => return Err(Error::InvalidEndpoint),
        };
        Ok(v)
    }
}

impl Display for PeerAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            PeerAddr::TcpIp { sock } => f.write_fmt(format_args!("tcpip({})", sock)),
            PeerAddr::Ws { url } => f.write_fmt(format_args!("ws({})", url)),
            PeerAddr::Browser { sock } => f.write_fmt(format_args!("browser({})", sock)),
        }
    }
}

/// Descriptor for how node to connect to another node on the network, or to
/// listen for connections from other nodes.  This parallels the structures in
/// `TransportCapability`.
#[derive(Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase", tag = "ty")]
pub enum ConnSpecifer {
    ConnectTcp { sock: net::SocketAddr },
    ListenTcp { sock: net::SocketAddr },
    ConnectWs { url: String },
    ListenWs { url: String },
}

pub struct PeerTicketData {
    addr: PeerAddr,
    drop_sig: Arc<Notify>,
    disconn_sig: Arc<Notify>,

    peer_inst: Mutex<Peer>,
}

/// Represents a "claim" to a peer connection.  As long as a peer ticket exists,
/// we'll keep trying to keep a connection with the peer it corresponds to alive.
///
/// Dropping the last reference to a PeerTicket schedules it to be disconnected.
pub type PeerTicket = Arc<PeerTicketData>;

impl PeerTicketData {
    pub fn from_parts(
        addr: PeerAddr,
        drop_sig: Arc<Notify>,
        disconn_sig: Arc<Notify>,
        peer: Peer,
    ) -> PeerTicket {
        let mut peer_inst = Mutex::new(peer);
        let ptd = PeerTicketData {
            addr,
            drop_sig,
            disconn_sig,
            peer_inst,
        };
        Arc::new(ptd)
    }

    /// Returns the remote address of the peer.
    pub fn addr(&self) -> &PeerAddr {
        &self.addr
    }

    /// Returns the current peer, or if not connected waits until we are.
    ///
    /// TODO Implement connection timeout handling.
    pub async fn get_peer(&self) -> Result<Peer, Error> {
        let pi = self.peer_inst.lock().await;
        Ok(pi.clone())
    }

    /// Waits until this connection drops, if it drops.  If you immediately
    /// call `.get_peer()` after this future completes then it will wait until
    /// the connection is reestablished, if it is.
    pub async fn wait_disconnected(&self) {
        self.disconn_sig.notified().await
    }
}

impl ::std::ops::Drop for PeerTicketData {
    fn drop(&mut self) {
        self.drop_sig.notify_waiters();
    }
}

pub struct ListenTicketData {
    drop_sig: Arc<Notify>,
}

/// Represents a claim to a listening socket.  Similar to a `PeerTicket`, except
/// it doesn't do very much.
///
/// Dropping it will stop listening.
pub type ListenTicket = Arc<ListenTicketData>;

impl ListenTicketData {
    pub fn from_notify(notify: Arc<Notify>) -> ListenTicket {
        let ltd = ListenTicketData { drop_sig: notify };
        Arc::new(ltd)
    }
}

impl ::std::ops::Drop for ListenTicketData {
    fn drop(&mut self) {
        self.drop_sig.notify_waiters();
    }
}

/// Connection handler used
pub type ConnHandler = Box<dyn Fn(PeerTicket) -> BoxFuture<'static, ()> + 'static + Sync + Send>;

pub fn make_basic_conn_handler<F>(fun: F) -> ConnHandler
where
    F: Fn(PeerTicket) + Sync + Send + 'static,
{
    let fa = Arc::new(fun);
    let handler = Box::new(move |tkt| {
        let fa2 = fa.clone();
        let fut = async move {
            fa2.as_ref()(tkt);
        };
        Box::pin(fut) as ::std::pin::Pin<Box<dyn Future<Output = ()> + Send>>
    });
    let b: ConnHandler = handler;
    b
}

/// Interface to platform specific peer management code.
///
/// There's likely only going to be two of these, one for normal desktop/server
/// code using Tokio and another for in browsers where we don't have that.
///
/// TODO Make sure this is the direction we actually want to go.
#[async_trait]
pub trait PeerController
where
    Self: Sync + Send,
{
    /// Returns the all capabilities we have access to.
    async fn get_caps(&self) -> Vec<TransportCapability>;

    /// Requests a ticket for a peer at an address, spawning tasks as necessary to service the peer.
    async fn connect(&self, _: PeerAddr) -> Result<PeerTicket, Error>;

    /// Listens on an address waiting for new connections, spawning tasks as necssary.
    ///
    /// The connection handler will be invoked for each connection, passing it
    /// the ticket for the peer.
    async fn listen(&self, _: PeerAddr, _: ConnHandler) -> Result<ListenTicket, Error>;
    /*
    /// Gets an iterator of new peer connections.
    ///
    /// TODO Fix type ergonomics.
    async fn get_peers_iter(&self) -> PeerStream;*/
}

pub type PeerCtl = Arc<dyn PeerController>;

/// Interface to platform specific peer connection.  If this is dropped then we
/// should implicitly close and clean up the connection.
#[async_trait]
pub trait PeerImpl
where
    Self: Sync + Send,
{
    /// Returns the peer's identity, if available.
    fn get_ident(&self) -> Option<crypto::PubIdent>;

    /// Returns if the connection is active.  If this is false then any IO calls
    /// will probably fail immediately.
    fn is_connected(&self) -> bool;

    /// Sends a packet, the returned future should block until it's sent.
    async fn send_packet(&self, _: &Packet) -> Result<(), Error>;

    /// Gracefully shuts down the connection, allowing this handle to be dropped
    /// without any side effects.
    async fn disconnect(&self) -> Result<(), Error>;

    /// Gets the stream of incoming packets from the peer.
    fn get_packet_stream<'p>(&'p self) -> Result<&'p PacketStream, Error>;
}

/// Peer handle, wrapped in an `Arc` for ergonomics.
pub type Peer = Arc<dyn PeerImpl>;

/// Utility implemenation so that we can have things that impl `PeerImpl` that
/// are in boxes or `Arc`s be `PeerImpl`.
#[async_trait]
impl<T, U> PeerImpl for T
where
    T: Deref<Target = U> + Sync + Send,
    U: PeerImpl + 'static,
{
    fn get_ident(&self) -> Option<crypto::PubIdent> {
        self.deref().get_ident()
    }

    fn is_connected(&self) -> bool {
        self.deref().is_connected()
    }

    async fn send_packet(&self, pkt: &Packet) -> Result<(), Error> {
        self.deref().send_packet(pkt).await
    }

    async fn disconnect(&self) -> Result<(), Error> {
        self.deref().disconnect().await
    }

    fn get_packet_stream<'p>(&'p self) -> Result<&'p PacketStream, Error> {
        self.deref().get_packet_stream()
    }
}

/// Utility function for working with peer implementations.  Make sure not to
/// accidentally end up calling this on a `Peer` since that introduces
/// unnecesary indirections.  This is actually almost a fixpoint function in
/// that case.
pub fn generalize_peer_impl<T: PeerImpl>(v: T) -> Peer
where
    T: 'static,
{
    Arc::new(v)
}

/// Temporary type used for ergonomics.
pub type PacketStreamInner = Box<dyn Stream<Item = Packet> + Send + Unpin>;

/// Temporary type used for ergonomics.
pub type PacketStream = Mutex<PacketStreamInner>;
