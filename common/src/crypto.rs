#![allow(unused)]

use serde_derive::{Deserialize, Serialize};

use sodiumoxide;
use sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as curve25519;
use sodiumoxide::crypto::sealedbox::curve25519blake2bxsalsa20poly1305 as sbcurve;
use sodiumoxide::crypto::secretbox::{self, xsalsa20poly1305 as xsalsa};
use sodiumoxide::crypto::sign::ed25519;

use crate::big_array::BigArray;

#[derive(Debug)]
pub enum Error {
    /// Deserialization failed, probably trying to parse an invalid struct.
    InvalidFormat,

    /// Invalid signature for some data.
    VerifyFail,
}

/// This is supposed to be the same as `xsalsa::MACBYTES` and
/// `curve25516::MACBYTES`, so that we only have to use one Tag type.
const TAG_BYTES: usize = 16usize;

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Tag(#[serde(with = "BigArray")] [u8; TAG_BYTES]);

/// Nonce used for symmetric encryption.  (NaCl secretbox)
#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Nonce(#[serde(with = "BigArray")] [u8; xsalsa::NONCEBYTES]);

impl Nonce {
    pub fn rand() -> Self {
        Self(xsalsa::gen_nonce().0)
    }

    pub fn raw(&self) -> &[u8] {
        &self.0[..]
    }
}

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct SecretKey(#[serde(with = "BigArray")] [u8; xsalsa::KEYBYTES]);

impl SecretKey {
    pub fn rand() -> Self {
        Self(xsalsa::gen_key().0)
    }

    pub fn encrypt(&self, msg: &[u8]) -> (Vec<u8>, Nonce, Tag) {
        let mut buf = Vec::from(msg);
        let (n, t) = self.encrypt_mut(buf.as_mut_slice());
        (buf, n, t)
    }

    pub fn encrypt_mut(&self, msg: &mut [u8]) -> (Nonce, Tag) {
        // This should optimize to be not terrible.
        let nonce = Nonce::rand();
        let nonce_raw = xsalsa::Nonce(nonce.0);
        let key_raw = xsalsa::Key(self.0);
        let tag_raw = xsalsa::seal_detached(msg, &nonce_raw, &key_raw);
        (nonce, Tag(tag_raw.0))
    }

    pub fn decrypt(&self, ciphertext: &[u8], nonce: &Nonce, tag: &Tag) -> Result<Vec<u8>, Error> {
        let mut buf = Vec::from(ciphertext);
        self.decrypt_mut(buf.as_mut_slice(), nonce, tag)
            .and_then(|_| Ok(buf))
    }

    pub fn decrypt_mut(&self, msg: &mut [u8], nonce: &Nonce, tag: &Tag) -> Result<(), Error> {
        let tag_raw = xsalsa::Tag(tag.0);
        let nonce_raw = xsalsa::Nonce(nonce.0);
        let key_raw = xsalsa::Key(self.0);
        xsalsa::open_detached(msg, &tag_raw, &nonce_raw, &key_raw).map_err(|_| Error::VerifyFail)
    }
}

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct EdPrvkey(#[serde(with = "BigArray")] [u8; ed25519::SECRETKEYBYTES]);

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct EdPubkey(#[serde(with = "BigArray")] [u8; ed25519::PUBLICKEYBYTES]);

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Signature(#[serde(with = "BigArray")] [u8; ed25519::SIGNATUREBYTES]);

impl EdPrvkey {
    pub fn rand_pair() -> (EdPubkey, Self) {
        let (p, k) = ed25519::gen_keypair();
        (EdPubkey(p.0), EdPrvkey(k.0))
    }

    pub fn sign(&self, msg: &[u8]) -> Signature {
        // This should optimize to be not terrible.
        let pk = ed25519::SecretKey(self.0);
        let raw_sig = ed25519::sign_detached(msg, &pk);
        Signature(raw_sig.0)
    }

    pub fn to_pubkey(&self) -> EdPubkey {
        let raw_sk = ed25519::SecretKey(self.0);
        let raw_pk = raw_sk.public_key();
        EdPubkey(raw_pk.0)
    }
}

impl EdPubkey {
    pub fn verify(&self, msg: &[u8], sig: &Signature) -> bool {
        // This should optimize to be not terrible.
        let raw_sig = ed25519::Signature(sig.0);
        let raw_key = ed25519::PublicKey(self.0);
        ed25519::verify_detached(&raw_sig, msg, &raw_key)
    }
}

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Seed(#[serde(with = "BigArray")] [u8; curve25519::SEEDBYTES]);

impl Seed {
    pub fn derive_keys(&self) -> (Pubkey, Prvkey) {
        let raw_seed = curve25519::Seed(self.0);
        let (raw_pk, raw_sk) = curve25519::keypair_from_seed(&raw_seed);
        (Pubkey(raw_pk.0), Prvkey(raw_sk.0))
    }
}

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Prvkey(#[serde(with = "BigArray")] [u8; curve25519::SECRETKEYBYTES]);

#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Pubkey(#[serde(with = "BigArray")] [u8; curve25519::PUBLICKEYBYTES]);

impl Prvkey {
    pub fn rand_pair() -> (Pubkey, Self) {
        let (p, k) = curve25519::gen_keypair();
        (Pubkey(p.0), Prvkey(k.0))
    }

    pub fn decrypt_direct(&self, ciphertext: &[u8]) -> Result<Vec<u8>, Error> {
        let raw_sk = curve25519::SecretKey(self.0);
        let derived_pk = raw_sk.public_key();
        sbcurve::open(ciphertext, &derived_pk, &raw_sk).map_err(|_| Error::VerifyFail)
    }

    pub fn encrypt_to(&self, msg: &[u8], dest: &Pubkey) -> (Vec<u8>, Nonce, Tag) {
        let mut buf = Vec::from(msg);
        let (n, t) = self.encrypt_to_mut(buf.as_mut_slice(), dest);
        (buf, n, t)
    }

    pub fn encrypt_to_mut(&self, msg: &mut [u8], dest: &Pubkey) -> (Nonce, Tag) {
        let nonce = Nonce::rand();
        let raw_n = curve25519::Nonce(nonce.0);
        let raw_sk = curve25519::SecretKey(self.0);
        let raw_pk = curve25519::PublicKey(dest.0);
        let raw_tag = curve25519::seal_detached(msg, &raw_n, &raw_pk, &raw_sk);
        (nonce, Tag(raw_tag.0))
    }

    pub fn decrypt_from(
        &self,
        msg: &[u8],
        tag: &Tag,
        nonce: &Nonce,
        src: &Pubkey,
    ) -> Result<Vec<u8>, Error> {
        let mut buf = Vec::from(msg);
        self.decrypt_from_mut(buf.as_mut_slice(), tag, nonce, src)
            .and_then(|_| Ok(buf))
    }

    pub fn decrypt_from_mut(
        &self,
        msg: &mut [u8],
        tag: &Tag,
        nonce: &Nonce,
        src: &Pubkey,
    ) -> Result<(), Error> {
        let raw_tag = curve25519::Tag(tag.0);
        let raw_n = curve25519::Nonce(nonce.0);
        let raw_sk = curve25519::SecretKey(self.0);
        let raw_pk = curve25519::PublicKey(src.0);
        curve25519::open_detached(msg, &raw_tag, &raw_n, &raw_pk, &raw_sk)
            .map_err(|_| Error::VerifyFail)
    }

    pub fn to_pubkey(&self) -> Pubkey {
        let raw_sk = curve25519::SecretKey(self.0);
        let raw_pk = raw_sk.public_key();
        Pubkey(raw_pk.0)
    }
}

impl Pubkey {
    pub fn encrypt_direct(&self, msg: &[u8]) -> Vec<u8> {
        let raw_pk = curve25519::PublicKey(self.0);
        sbcurve::seal(msg, &raw_pk)
    }
}

/// Private keys used for signing messages and decrypting stuff, I guess.
#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Identity {
    pub encryption_key: Prvkey,
    pub sigining_key: EdPrvkey,
}

impl Identity {
    pub fn rand() -> Identity {
        let (_, ek) = Prvkey::rand_pair();
        let (_, sk) = EdPrvkey::rand_pair();
        Identity {
            encryption_key: ek,
            sigining_key: sk,
        }
    }

    pub fn to_pub(&self) -> PubIdent {
        let ep = self.encryption_key.to_pubkey();
        let sp = self.sigining_key.to_pubkey();

        let sig_msg = gen_pubident_sig_msg(&ep, &sp);
        let sig = self.sigining_key.sign(sig_msg.as_slice());

        PubIdent {
            encryption_pubkey: ep,
            signing_pubkey: sp,
            auth_sig: sig,
        }
    }
}

/// Public keys corresponding to an identity, to be shared freely.  Includes a
/// signature to ensure that the keys are authentic.
///
/// TODO Find a better name for this struct.
#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct PubIdent {
    pub encryption_pubkey: Pubkey,
    pub signing_pubkey: EdPubkey,

    /// This is just the above two fields as bytes, concatenated.  See in
    /// `gen_pubident_sig_msg` for concrete logic.  We may add more fields to
    /// this struct so it's useful to abstract this out.
    auth_sig: Signature,
}

#[inline(always)]
fn gen_pubident_sig_msg(epk: &Pubkey, spk: &EdPubkey) -> Vec<u8> {
    let mut buf: Vec<u8> = Vec::new();
    buf.extend(&epk.0[..]);
    buf.extend(&spk.0[..]);
    buf
}

impl PubIdent {
    fn verify(&self) -> Result<(), Error> {
        let sig_msg = gen_pubident_sig_msg(&self.encryption_pubkey, &self.signing_pubkey);
        let ok = self
            .signing_pubkey
            .verify(sig_msg.as_slice(), &self.auth_sig);
        if ok {
            Ok(())
        } else {
            Err(Error::VerifyFail)
        }
    }
}

#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct IdentityContainer {
    enc_id: Vec<u8>,
    nonce: Nonce,
    tag: Tag,
}

impl IdentityContainer {
    pub fn encrypt(ident: &Identity, id_unlock_key: &SecretKey) -> Self {
        let mut buf = rmp_serde::to_vec(ident).expect("ident serialization");
        let (nonce, tag) = id_unlock_key.encrypt_mut(buf.as_mut_slice());
        IdentityContainer {
            enc_id: buf,
            nonce,
            tag,
        }
    }

    pub fn decrypt(&self, id_unlock_key: &SecretKey) -> Result<Identity, Error> {
        let mut buf = self.enc_id.clone();
        id_unlock_key
            .decrypt_mut(buf.as_mut_slice(), &self.nonce, &self.tag)
            .map_err(|_| Error::VerifyFail)?;
        let ident = rmp_serde::from_slice(buf.as_slice()).map_err(|_| Error::InvalidFormat)?;
        Ok(ident)
    }
}

/// Utility routine to initialize CSPRNG state, etc.
pub fn init() -> bool {
    sodiumoxide::init().is_ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Once;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            init();
        })
    }

    #[test]
    fn test_sign_verify() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p, k) = EdPrvkey::rand_pair();

        let sig = k.sign(msg);
        assert!(p.verify(msg, &sig));
    }

    #[test]
    fn test_sign_verify_fail() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p, k) = EdPrvkey::rand_pair();

        let mut sig = k.sign(msg);
        sig.0[0] ^= 0xff;
        assert!(!p.verify(msg, &sig));
    }

    #[test]
    fn test_symm_enc_dec() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let k = SecretKey::rand();

        let (c, n, t) = k.encrypt(msg);
        let m = k
            .decrypt(c.as_slice(), &n, &t)
            .expect("secretbox decrypt fail");

        assert_eq!(m.as_slice(), msg);
    }

    #[test]
    fn test_symm_enc_dec_fail() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let k = SecretKey::rand();

        let (c, n, mut t) = k.encrypt(msg);
        t.0[0] ^= 0xff;
        let res = k.decrypt(c.as_slice(), &n, &t);

        assert!(res.is_err());
    }

    #[test]
    fn test_dh_enc_dec() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p1, k1) = Prvkey::rand_pair();
        let (p2, k2) = Prvkey::rand_pair();

        let (c, n, t) = k1.encrypt_to(msg, &p2);
        let m = k2
            .decrypt_from(c.as_slice(), &t, &n, &p1)
            .expect("box decrypt fail");

        assert_eq!(m.as_slice(), msg);
    }

    #[test]
    fn test_dh_enc_dec_fail() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p1, k1) = Prvkey::rand_pair();
        let (p2, k2) = Prvkey::rand_pair();

        let (c, n, mut t) = k1.encrypt_to(msg, &p2);
        t.0[0] ^= 0xff;
        let res = k2.decrypt_from(c.as_slice(), &t, &n, &p1);

        assert!(res.is_err());
    }

    #[test]
    fn test_direct_enc_dec() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p, k) = Prvkey::rand_pair();

        let ciphertext = p.encrypt_direct(msg);
        let m = k
            .decrypt_direct(ciphertext.as_slice())
            .expect("sealedbox decrypt fail");

        assert_eq!(m.as_slice(), msg);
    }

    #[test]
    fn test_direct_enc_dec_fail() {
        setup();

        let msg: &[u8] = &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let (p, k) = Prvkey::rand_pair();

        let mut ciphertext = p.encrypt_direct(msg);
        ciphertext[0] ^= 0xff;
        let res = k.decrypt_direct(ciphertext.as_slice());

        assert!(res.is_err());
    }

    #[test]
    fn test_identity_verify() {
        setup();

        let ident = Identity::rand();
        let pi = ident.to_pub();

        assert!(pi.verify().is_ok());
    }

    #[test]
    fn test_identity_verify_fail_sig() {
        setup();

        let ident = Identity::rand();
        let mut pi = ident.to_pub();
        pi.auth_sig.0[0] ^= 0xff;

        assert!(pi.verify().is_err());
    }

    #[test]
    fn test_identity_verify_fail_keys() {
        setup();

        let ident = Identity::rand();
        let mut pi1 = ident.to_pub();
        let mut pi2 = ident.to_pub();
        pi1.encryption_pubkey.0[0] ^= 0xff;
        pi2.signing_pubkey.0[0] ^= 0xff;

        assert!(pi1.verify().is_err());
        assert!(pi2.verify().is_err());
    }
}
