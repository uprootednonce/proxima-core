use std::fmt::{self, Debug, Display};

use hex;
use serde_derive::{Deserialize, Serialize};

use crate::big_array::BigArray;
use crate::crypto;

#[derive(Debug)]
pub enum Error {
    /// If a bundle was invalid due to a bad signature or some other malformedness.
    InvalidBundle,
}

/// Uniquely identifies a stream when bookkeeping.  Should be a truncated hash
/// of a pubkey or something.
#[derive(Copy, Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct StreamId(#[serde(with = "BigArray")] [u8; 16]);

impl StreamId {
    pub fn new_rand() -> StreamId {
        // TODO Make this use an actual dedicated RNG.
        let n1 = crypto::Nonce::rand();
        let n2 = crypto::Nonce::rand();
        let mut buf = [0; 16];

        // This is a little contrived, we should make it less so.
        n1.raw()
            .iter()
            .chain(n2.raw().iter())
            .enumerate()
            .take(buf.len())
            .for_each(|(i, n)| buf[i] = *n);

        StreamId(buf)
    }
}

impl Display for StreamId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let hex = hex::encode(&self.0);
        f.write_str(hex.as_str())
    }
}

/// Metadata used for tracking a bundle.
#[derive(Copy, Clone, Hash, Debug, Deserialize, Serialize)]
pub struct BundleMeta {
    /// Copy of the stream ID to verify the commitment in the wrapper.
    sid: StreamId,

    /// Copy of the bundle ID to verify the commitment in the wrapper.
    bid: u64,

    /// UTC milliseconds UNIX timestamp of when this bundle was created, just
    /// before packaging and signing.  Also can be used to prevent replays by
    /// rejecting bundles that are too old.
    ts: u64,
}

/// Contains application layer streaming information.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct Bundle {
    /// Metadata for tracking this bundle.
    meta: BundleMeta,

    /// Commands to be processed by the decoder, see `encdec`.
    ///
    /// should be interpreted in the order they are listed.
    cmds: Vec<Command>,
}

impl Bundle {
    pub fn new(sid: StreamId, bid: u64, ts: u64, cmds: Vec<Command>) -> Bundle {
        let meta = BundleMeta { sid, bid, ts };
        Bundle { meta, cmds }
    }

    pub fn meta(&self) -> &BundleMeta {
        &self.meta
    }

    pub fn sid(&self) -> &StreamId {
        &self.meta.sid
    }

    pub fn bid(&self) -> u64 {
        self.meta.bid
    }

    pub fn timestamp(&self) -> u64 {
        self.meta.ts
    }

    pub fn commands(&self) -> &Vec<Command> {
        &self.cmds
    }

    pub fn take_commands(self) -> Vec<Command> {
        self.cmds
    }
}

#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase", tag = "cty")]
pub enum Command {
    Msg(Message),
    //UpdateKey(UpdateKeyCmd),
}

/// Information to set up author key rotatation.
///
/// Not yet implemented.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct UpdateKeyCmd {
    // FIXME Analyze security properties of tying keys to time periods.  We
    // don't use BIDs here in order to account for dropped bundles.
    after_ts: u64,
    new_author_pubkey: crypto::Pubkey,
}

/// Messages should be passed up to the higher layer client implementation.
/// These usually carry stream payload data or other user-facing information.
///
/// Will thing of a better name for this later.
#[derive(Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase", tag = "mty")]
pub enum Message {
    /// Announcement message that should be logged to somewhere the user can
    /// see.  This is struct-style because serde apparently doesn't like it as a
    /// newtype style for some reason.
    Announce { body: String },

    /// New chunk of the stream that should be processed.
    Chunk(Chunk),

    /// Message that the stream has ended and we should exit gracefully.
    EndStream,
}

/// Wrapper struct to contain the segments of a stream with associated
/// sequencing and channel data.
#[derive(Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Chunk {
    /// Channel identifier to differentiate multiple embedded content streams.
    chid: u64,

    /// Sequence number.
    ///
    /// TODO Somehow encode timestamp information.
    seq: u64,

    /// Duration in milliseconds.
    dur: u64,

    /// HLS .ts chunk, although we might expand this to other types eventually.
    #[serde(with = "serde_bytes")]
    buf: Vec<u8>,
}

impl Chunk {
    pub fn new(chid: u64, seq: u64, dur: u64, buf: Vec<u8>) -> Chunk {
        Chunk {
            chid,
            seq,
            dur,
            buf,
        }
    }

    /// Channel ID to between channels within a stream.
    pub fn channel_id(&self) -> u64 {
        self.chid
    }

    /// Sequence number.
    pub fn seq_no(&self) -> u64 {
        self.seq
    }

    /// Duration in milliseconds.
    pub fn duration(&self) -> u64 {
        self.dur
    }

    /// Extract the actual contents of the chunk
    pub fn into_contents(self) -> Vec<u8> {
        self.buf
    }
}

/// External contents of a bundle after decoding it.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct BundleManifest {
    meta: BundleMeta,
    msgs: Vec<Message>,
}

impl BundleManifest {
    pub fn new(meta: BundleMeta, msgs: Vec<Message>) -> BundleManifest {
        BundleManifest { meta, msgs }
    }

    pub fn meta(&self) -> &BundleMeta {
        &self.meta
    }

    pub fn sid(&self) -> &StreamId {
        &self.meta.sid
    }

    pub fn bid(&self) -> u64 {
        self.meta.bid
    }

    pub fn timestamp(&self) -> u64 {
        self.meta.ts
    }

    pub fn messages(&self) -> &Vec<Message> {
        &self.msgs
    }
}
