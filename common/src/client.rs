#![allow(unused)]

use std::sync::Arc;

use async_trait::async_trait;
use futures::StreamExt;
use serde_derive::{Deserialize, Serialize};
use tokio::sync::{mpsc, Mutex, Notify};
use tokio::task;

use crate::crypto;
use crate::discovery;
use crate::encdec;
use crate::p2p;
use crate::router;
use crate::stream;

const ROUTE_BUF_SIZE: usize = 8;

#[derive(Debug)]
pub enum Error {
    /// If we get network traffic for a stream we don't care about.
    StreamMismatch,

    /// If we try to write to the display and something wrong happens.
    DisplayStateError,

    /// Some not-immediately-recoverable network error.
    Net(p2p::Error),

    /// Some bundle problem we can't recover from here.
    BadBundle(encdec::Error),
}

impl ::std::convert::From<p2p::Error> for Error {
    fn from(f: p2p::Error) -> Self {
        Error::Net(f)
    }
}

/// Output device to write to the user various stream messages.
#[async_trait]
pub trait ClientOutputImpl {
    /// Checks if the screen is active.  If we call `.end_stream()`, then calls
    /// to this should return false after that future completes.
    fn is_active(&self) -> bool;

    /// Displays some notification to the user.
    async fn send_announcement(&self, _: String) -> Result<(), Error>;

    /// Queues the chunk to be rendered to the screen to the user.  The sequence
    /// numbers on these chunks might not be in-order so that should be handled
    /// in some reasonable way.
    async fn write_chunk(&self, _: stream::Chunk) -> Result<(), Error>;

    /// Starts the process to gracefully end the stream, optionally waiting on it.
    async fn end_stream(&self, _: bool) -> Result<(), Error>;

    /// Waits for the stream to finish playing any queued video data.
    async fn wait_finished(&self) -> Result<(), Error>;
}

pub type ClientOutput = Box<dyn ClientOutputImpl + Sync + Send + 'static>;

/// High level client for watching a particular stream.
pub struct Client {
    /// Static user identity.
    user_id: crypto::Identity,

    /// Fixed metadata about the stream.
    stream_desc: discovery::StreamDesc,

    /// Decoder configured with
    bundle_decoder: Mutex<encdec::Decoder>,

    /// Peer control interface.
    peer_ctl: p2p::PeerCtl,

    /// Display interface for the user.
    display_ctl: ClientOutput,

    /// Input for bundles to be relayed to other peers.
    route_input: mpsc::Sender<router::RoutingRequest>,

    /// Handle to update our local routing decisions based on directions from
    /// the caster.
    local_router_handle: router::RouterHandle,

    /// Used to stop the client later.
    stop_sig: Notify,

    /// Used to notify waiters that we've exited.
    exit_sig: Notify,
}

impl Client {
    /// Spawns a task for the client and returns a handle to interact with it.
    pub async fn start(
        ident: crypto::Identity,
        desc: discovery::StreamDesc,
        pc: p2p::PeerCtl,
        disp: ClientOutput,
    ) -> Result<Arc<Client>, Error> {
        // Set up components.
        let decoder = desc.make_decoder();
        let (rh, r_tx) = router::RouterHandle::start().await;

        let client = Client {
            user_id: ident,
            stream_desc: desc,
            bundle_decoder: Mutex::new(decoder),
            peer_ctl: pc,
            display_ctl: disp,
            route_input: r_tx,
            local_router_handle: rh,
            stop_sig: Notify::new(),
            exit_sig: Notify::new(),
        };

        // Try to connect right now, if it doesn't work then fail.
        let caster_tkt = init_streaming(&client).await?;
        let client = Arc::new(client);
        task::spawn(do_client_main(client.clone(), caster_tkt));

        Ok(client)
    }

    /// If the client is running, signals it to stop.
    pub async fn stop(self: &Arc<Self>) -> Result<(), Error> {
        // TOOD State checking.
        self.stop_sig.notify_waiters();
        Ok(())
    }

    /// If the client is running, waits until it finishes.
    pub async fn wait(self: &Arc<Self>) -> Result<(), Error> {
        // TOOD State checking.
        self.exit_sig.notified().await;
        Ok(())
    }
}

/// Connects to a server and subscribes to the stream.
async fn init_streaming(cli: &Client) -> Result<p2p::PeerTicket, Error> {
    let addr = cli.stream_desc.caster_addr();
    eprintln!("[streamclient] Attempting to start stream from {:?}", addr);

    // Connect to the host and request we start the stream.
    let src_tkt: p2p::PeerTicket = cli.peer_ctl.connect(addr.clone()).await?;
    let src_peer: p2p::Peer = src_tkt.get_peer().await?;
    let upd = p2p::SubState {
        stream_id: cli.stream_desc.stream_id().clone(),
        state: true,
    };
    let req = p2p::Packet::SubUpdate(upd);
    src_peer.send_packet(&req).await?;

    Ok(src_tkt)
}

async fn handle_pkt_pushbundle<'p>(
    cli: &'p Client,
    bund: &encdec::PackedBundle,
    peer: &p2p::Peer,
) -> Result<(), Error> {
    // Verify the bundle is for this stream.
    // TODO Do we want to move this check to the decoder?
    if bund.sid() != cli.stream_desc.stream_id() {
        eprintln!("[streamclient] [warn] Got bundle for stream we didn't want");
        return Err(Error::StreamMismatch);
    }

    // Decode the bundle, we may have already gotten this so possible exit now.
    let mut decoder = cli.bundle_decoder.lock().await;
    let mf: stream::BundleManifest = match decoder.process_bundle(&bund) {
        Ok(mf) => mf,
        // This is actually ok, just ignore it.
        Err(encdec::Error::ReplayedBundle) => {
            eprintln!("[streamclient] [warn] Got replayed bundle, possible routing loop, dropping");
            return Ok(());
        }
        // Other errors we should report.
        Err(e) => {
            return Err(Error::BadBundle(e));
        }
    };

    eprintln!(
        "[streamclient] Got bundle id {}, ts {}",
        mf.bid(),
        mf.timestamp()
    );

    // Queue it to be relayed now that we know it's ok.
    let rreq = router::RoutingRequest::Relay(peer.clone(), bund.clone());
    if cli.route_input.send(rreq).await.is_err() {
        eprintln!("[streamclient] Router unexpectedly shut down!  This is probably a bug!");
    }

    // Now send off the bundle messages to the output.
    for msg in mf.messages() {
        let dc = cli.display_ctl.as_ref();
        let res = match msg {
            stream::Message::Announce { body } => dc.send_announcement(body.clone()).await,
            stream::Message::Chunk(chunk) => dc.write_chunk(chunk.clone()).await,
            stream::Message::EndStream => dc.end_stream(true).await,
        };

        // Same error type so let's handle the result here.
        match res {
            Ok(_) => {}
            Err(e) => {
                eprintln!("[streamclient] Error processing bundle: {:?}", e);
                return Err(e);
            }
        }
    }

    Ok(())
}

async fn handle_packet<'p>(
    cli: &'p Client,
    pkt: &'p p2p::Packet,
    tkt: &p2p::PeerTicket,
    peer: &p2p::Peer,
) -> Result<(), Error> {
    match pkt {
        p2p::Packet::PushBundle(bund) => handle_pkt_pushbundle(cli, bund, peer).await?,
        _ => {
            eprintln!("[streamclient] Ignoring packet from {}", tkt.addr());
        }
    }

    Ok(())
}

/// Handles packets coming from a peer.
///
/// TODO Make this handle reconnections properly.
async fn do_handle_packets_worker(cli: &Client, peer_tkt: p2p::PeerTicket) -> Result<(), Error> {
    let addr = peer_tkt.addr();

    // This would probably have to be moved inside another loop if we want to
    // make it handle reconnections.
    let peer = peer_tkt.get_peer().await?;
    let pkts_lock = peer.get_packet_stream()?;
    let mut pkts = pkts_lock.lock().await;

    while let Some(pkt) = pkts.as_mut().next().await {
        handle_packet(cli, &pkt, &peer_tkt, &peer).await?;
    }

    eprintln!("[streamclient] Packet ingest task for {:?} exited", addr);
    Ok(())
}

async fn do_client_main(client: Arc<Client>, caster_tkt: p2p::PeerTicket) {
    // Just wait for one to complete.
    let pkts_fut = do_handle_packets_worker(client.as_ref(), caster_tkt);
    let stop_fut = client.stop_sig.notified();

    futures::pin_mut!(pkts_fut);
    futures::pin_mut!(stop_fut);
    futures::future::select(pkts_fut, stop_fut).await;

    eprintln!("[streamclient] Client main exited.");
    client.exit_sig.notify_waiters();
}
