#![allow(unused)]

use std::collections::*;
use std::time;

use serde_derive::{Deserialize, Serialize};

use crate::crypto;
use crate::p2p;
use crate::stream;

use stream::{Command, Message};

#[derive(Debug)]
pub enum Error {
    /// If the bundle is malformed in some way.
    InvalidBundle,

    /// If the embedded SID/BID commitments don't match those in the wrapper.
    CommitmentMismatch,

    /// If we tried to re-process a bundle we already processed.
    ReplayedBundle,
}

/// Like the below, but what the author uses to pack bundles.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct StreamEncoderDesc {
    pub author_prvkey: crypto::Prvkey,
    pub stream_pubkey: crypto::Pubkey,
}

/// High level information for digesting a media stream.  Used to decrypt and
/// validate bundles and other messages.
///
/// More things will be needed here eventually.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct StreamDecoderDesc {
    pub author_pubkey: crypto::Pubkey,
    pub stream_prvkey: crypto::Prvkey,
}

/// Creates a pair of descriptors for encoding/decoding the stream.  This will
/// be removed eventually when we expand the key management logic.
pub fn make_desc_pair(
    author_key: crypto::Prvkey,
    stream_key: crypto::Prvkey,
) -> (StreamEncoderDesc, StreamDecoderDesc) {
    let ap = author_key.to_pubkey();
    let sp = stream_key.to_pubkey();
    let sed = StreamEncoderDesc {
        author_prvkey: author_key,
        stream_pubkey: sp,
    };
    let sdd = StreamDecoderDesc {
        author_pubkey: ap,
        stream_prvkey: stream_key,
    };
    (sed, sdd)
}

/// Signed and encrypted bundle for transport between peers.
///
/// We duplicate some information here and then do an extra evaluation step on
/// it that isn't strictly necessary, so we should define a better way to deal
/// with that that splits apart the encrypted and signed data from the
/// signed-only data.
#[derive(Clone, Hash, Debug, Deserialize, Serialize)]
pub struct PackedBundle {
    /// Stream ID to help route the packed bundle.  This stays persistent across
    /// the lifetime of the stream.
    sid: stream::StreamId,

    /// Bundle ID to be used to avoid replay attacks and avoid re-relaying
    /// duplicate bundles.  Does not have to be sequential.
    // TODO Make this a newtype, probably.
    bid: u64,

    /// Nonce used to encrypt bundle buffer.
    nonce: crypto::Nonce,

    /// Tag to authenticate encryption.
    tag: crypto::Tag,

    /// Encrypted bundle.
    buf: Vec<u8>,
}

impl PackedBundle {
    pub fn sid(&self) -> stream::StreamId {
        self.sid
    }
}

/// Packs a bundle, encrypting it from the author to the stream key.  Returns an
/// error if the bundle has a type we can't serialize for some reason.
pub fn pack_bundle(
    bundle: &stream::Bundle,
    author_sk: &crypto::Prvkey,
    stream_pk: &crypto::Pubkey,
) -> Result<PackedBundle, Error> {
    let sid = *bundle.sid();
    let bid = bundle.bid();
    let mut buf = rmp_serde::to_vec(bundle).map_err(|_| Error::InvalidBundle)?;
    let (nonce, tag) = author_sk.encrypt_to_mut(buf.as_mut_slice(), stream_pk);
    Ok(PackedBundle {
        sid,
        bid,
        nonce,
        tag,
        buf,
    })
}

/// Tries to decrypt and validate the packed bundle.  Errors if the bundle is
/// invalid in any way.
pub fn unpack_bundle(
    pack: &PackedBundle,
    author_pk: &crypto::Pubkey,
    stream_sk: &crypto::Prvkey,
) -> Result<stream::Bundle, Error> {
    let sid = pack.sid;
    let bid = pack.bid;

    // Decrypt and deserialize the internal buffer.
    let mut buf = pack.buf.clone();
    stream_sk
        .decrypt_from_mut(buf.as_mut_slice(), &pack.tag, &pack.nonce, &author_pk)
        .map_err(|_| Error::InvalidBundle)?;
    let bund: stream::Bundle =
        rmp_serde::from_slice(buf.as_slice()).map_err(|_| Error::InvalidBundle)?;

    // Verify the commitments.
    if sid != *bund.sid() {
        return Err(Error::CommitmentMismatch);
    }

    if bid != bund.bid() {
        return Err(Error::CommitmentMismatch);
    }

    Ok(bund)
}

/// Used to author bundles for distribution.
pub struct Encoder {
    /// Stream ID to stamp bundles with.
    stream_id: stream::StreamId,

    /// Keys used to encrypt and authenticate produced bundles.
    stream_desc: StreamEncoderDesc,

    /// Next bundle ID
    /// TODO Should this be unpredictable?
    next_bid: u64,
}

/// Set of messages to be made into a packed bundle later.
#[derive(Clone)]
pub struct BundleRequest {
    cmds: Vec<stream::Command>,
}

impl BundleRequest {
    pub fn new(cmds: Vec<stream::Command>) -> BundleRequest {
        BundleRequest { cmds }
    }
}

impl Encoder {
    pub fn new(stream_id: stream::StreamId, enc_desc: StreamEncoderDesc) -> Encoder {
        Encoder {
            stream_id: stream_id,
            stream_desc: enc_desc,
            next_bid: 0,
        }
    }

    pub fn create_packed_bundle(&mut self, req: BundleRequest) -> Result<PackedBundle, Error> {
        let bid = self.next_bid;
        self.next_bid += 1;

        let ts = {
            let now = time::SystemTime::now();
            let dur = now
                .duration_since(time::UNIX_EPOCH)
                .expect("time went backwards");
            dur.as_secs() * 1000 + dur.subsec_millis() as u64
        };

        let bund = stream::Bundle::new(self.stream_id, bid, ts, req.cmds);
        let keys = &self.stream_desc;
        let packed = pack_bundle(&bund, &keys.author_prvkey, &keys.stream_pubkey)
            .map_err(|_| Error::InvalidBundle)?;

        Ok(packed)
    }
}

/// Used to process incoming bundles and write chunks to the screen.
pub struct Decoder {
    /// Stream ID to verify bundles with.
    stream_id: stream::StreamId,

    /// Keys for decoding bundles.
    stream_desc: StreamDecoderDesc,

    /// Maps bundle IDs to their timestamps.
    bid_ts_map: HashMap<u64, u64>,
}

impl Decoder {
    pub fn new(stream_id: stream::StreamId, dec_desc: StreamDecoderDesc) -> Decoder {
        Decoder {
            stream_id: stream_id,
            stream_desc: dec_desc,
            bid_ts_map: HashMap::new(),
        }
    }

    /// Processes a bundle, executing any decoder commands, feeding messages to
    /// our output, and returning the bundle ID for bookkeeping purposes.
    pub fn process_bundle(
        &mut self,
        pbundle: &PackedBundle,
    ) -> Result<stream::BundleManifest, Error> {
        let keys = &self.stream_desc;

        let bundle = unpack_bundle(pbundle, &keys.author_pubkey, &keys.stream_prvkey)
            .map_err(|_| Error::InvalidBundle)?;
        let bid = bundle.bid();

        // Check if we've seen it already.
        if self.bid_ts_map.contains_key(&bid) {
            return Err(Error::ReplayedBundle);
        }

        // Insert the BID into the map.
        // TODO Remove these later, after they expire.  We can probably do this
        // in this function since it should work out that we only need to remove
        // roughly one at a time depending on the source's stability.
        self.bid_ts_map.insert(bid, bundle.timestamp());

        // Things to later store in the manifest.
        let meta = *bundle.meta();
        let mut msgs = Vec::new();

        // Only thing we really have to do right now is blindly queue the
        // messages to the output.
        for cmd in bundle.take_commands() {
            match cmd {
                stream::Command::Msg(m) => {
                    msgs.push(m);
                }
                _ => unimplemented!(),
            }
        }

        /// Return the manifest for later processing.
        Ok(stream::BundleManifest::new(meta, msgs))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Once;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            crypto::init();
        })
    }

    #[test]
    fn test_encdec_basic() {
        setup();

        let sid = stream::StreamId::new_rand();
        let (_, ak) = crypto::Prvkey::rand_pair();
        let (_, sk) = crypto::Prvkey::rand_pair();
        let (sed, sdd) = make_desc_pair(ak, sk);

        let mut enc = Encoder::new(sid, sed);
        let mut dec = Decoder::new(sid, sdd);

        let mut msgs = Vec::new();
        let mut packeds = Vec::new();
        let mut unpackeds = Vec::new();

        for i in 0..16u8 {
            let msg = Message::Announce {
                body: format!("hello! {}", i),
            };
            msgs.push(msg.clone());
            let req = BundleRequest::new(vec![Command::Msg(msg)]);
            let pb = enc.create_packed_bundle(req).expect("encode bundle");
            packeds.push(pb);
        }

        for pb in packeds {
            let mf = dec.process_bundle(&pb).expect("decode bundle");

            // FIXME Clone.
            unpackeds.extend(mf.messages().iter().cloned());
        }

        assert_eq!(msgs, unpackeds);
    }
}
