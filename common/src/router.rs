#![allow(unused)]

use std::collections::*;
use std::convert::AsMut;
use std::sync::atomic::AtomicU64;
use std::sync::Arc;

use tokio::sync::{mpsc, Mutex};
use tokio::task;

use serde_derive::{Deserialize, Serialize};

use crate::encdec;
use crate::p2p;

/// Packet queued to be routed, with information on its origin.
pub enum RoutingRequest {
    New(encdec::PackedBundle),
    Relay(p2p::Peer, encdec::PackedBundle),
}

impl RoutingRequest {
    /// If this request came from a peer, returns the peer.
    pub fn peer(&self) -> Option<&p2p::Peer> {
        match self {
            Self::New(_) => None,
            Self::Relay(p, _) => Some(p),
        }
    }

    pub fn bundle(&self) -> &encdec::PackedBundle {
        match self {
            Self::New(b) => b,
            Self::Relay(_, b) => b,
        }
    }
}

pub struct RouterHandle {
    state: Arc<RouterState>,
}

const ROUTE_BUFFER_SIZE: usize = 32;

impl RouterHandle {
    pub async fn start() -> (RouterHandle, mpsc::Sender<RoutingRequest>) {
        let (tx, rx) = mpsc::channel(ROUTE_BUFFER_SIZE);

        let st = RouterState {
            bundles_routed: AtomicU64::new(0),
            tbl: Mutex::new(RoutingTable {
                downstreams: Vec::new(),
            }),
        };

        let state = Arc::new(st);
        let rh = RouterHandle {
            state: state.clone(),
        };

        // Start the task to route the bundles!
        task::spawn(do_router_worker(state, rx));

        (rh, tx)
    }

    pub async fn add_downstream(&self, peer: &p2p::Peer) {
        let mut tbl = self.state.tbl.lock().await;
        if !tbl.downstreams.iter().any(|p| Arc::ptr_eq(peer, p)) {
            tbl.downstreams.push(peer.clone());
        }
    }

    pub async fn remove_downstream(&self, peer: &p2p::Peer) -> bool {
        let mut tbl = self.state.tbl.lock().await;

        // Find the index of the peer in the vec.
        let idx = tbl
            .downstreams
            .iter()
            .enumerate()
            .filter(|(i, p)| Arc::ptr_eq(peer, p))
            .map(|(i, _)| i)
            .next();

        if let Some(idx) = idx {
            tbl.downstreams.swap_remove(idx);
            true
        } else {
            false
        }
    }
}

/// Common state for the router.
struct RouterState {
    bundles_routed: AtomicU64,
    tbl: Mutex<RoutingTable>,
}

struct RoutingTable {
    downstreams: Vec<p2p::Peer>,
}

async fn route_bundle(bundle: &encdec::PackedBundle, tbl: &RoutingTable) {
    if tbl.downstreams.is_empty() {
        return;
    }

    // TODO See if we can avoid cloning here.
    let pkt = p2p::Packet::PushBundle(bundle.clone());
    for peer in tbl.downstreams.iter() {
        match peer.send_packet(&pkt).await {
            Ok(_) => {}
            Err(e) => {
                eprintln!("[router] failure to relay bundle: {:?}", e)
            }
        }
    }
}

/// Main future for router tasks.
async fn do_router_worker(
    state: Arc<RouterState>,
    mut route_queue: mpsc::Receiver<RoutingRequest>,
) {
    loop {
        if let Some(rq) = route_queue.recv().await {
            // TODO Verify we haven't received this bundle before to avoid
            // causing a routing loop and DoSing ourselves.
            let bundle = rq.bundle();

            // Now send the bundle off to other peers.
            let tbl = state.tbl.lock().await;
            route_bundle(&bundle, &tbl).await;
        }
    }
}

/// Specifies the direction of a connection, referring to the direction of
/// bundle flow.  These are relative to the node, so really you relay packets
/// towards the downstreams unconditionally.
///
/// TODO Do we actually need this?
#[derive(Copy, Clone, Hash, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum FlowDir {
    Downstream,
    Upstream,
}
